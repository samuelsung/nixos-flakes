{ lib, config, pkgs, ... }:

with lib;
let
  cfg = config.systemCustomize;

  encryptedJson = (builtins.fromJSON (
    builtins.readFile cfg.gitlab-runner.sopsFile
  )).gitlab-runner;

  # NOTICE(samuelsung): it seems that old runners will fail to be unregistered if the service name gets to long...
  # "repo" is not need to be exact, can be shorten if to long
  getServiceName = repo: tag: "${repo}-${tag}";

  getSopsKey = repo: tag: "${repo}-${tag}-gr";

  flattenNames = flatten (
    mapAttrsToList
      (tag: repos:
        (mapAttrsToList
          (repo: _: { inherit tag repo; })
          repos)
      )
      encryptedJson
  );

  genNixRunner = { registrationConfigFile }: {
    inherit registrationConfigFile;
    dockerImage = "alpine";
    dockerVolumes = [
      "/nix/store:/nix/store:ro"
      "/nix/var/nix/db:/nix/var/nix/db:ro"
      "/nix/var/nix/daemon-socket:/nix/var/nix/daemon-socket:ro"
      "${config.sops.secrets.no_scope_github_token.path}:${config.sops.secrets.no_scope_github_token.path}:ro"
    ];
    dockerDisableCache = true;
    preBuildScript = pkgs.writeScript "setup-container" ''
      mkdir -p -m 0755 /nix/var/log/nix/drvs
      mkdir -p -m 0755 /nix/var/nix/gcroots
      mkdir -p -m 0755 /nix/var/nix/profiles
      mkdir -p -m 0755 /nix/var/nix/temproots
      mkdir -p -m 0755 /nix/var/nix/userpool
      mkdir -p -m 1777 /nix/var/nix/gcroots/per-user
      mkdir -p -m 1777 /nix/var/nix/profiles/per-user
      mkdir -p -m 0755 /nix/var/nix/profiles/per-user/root
      mkdir -p -m 0700 "$HOME/.nix-defexpr"
      mkdir -p -m 0755 /etc/nix
      cat <<EOF > /etc/nix/nix.conf
      experimental-features = nix-command flakes
      access-tokens = github.com=''$(cat ${config.sops.secrets.no_scope_github_token.path})
      EOF
      . ${pkgs.nixFlakes}/etc/profile.d/nix.sh
      ${pkgs.nixFlakes}/bin/nix-env -i ${concatStringsSep " " (with pkgs; [
        nixFlakes
        cacert
        git
        openssh
      ])}
      ${pkgs.nixFlakes}/bin/nix-channel --add ${config.system.defaultChannel} nixpkgs
      ${pkgs.nixFlakes}/bin/nix-channel --update nixpkgs
    '';
    environmentVariables = {
      ENV = "/etc/profile";
      USER = "root";
      NIX_REMOTE = "daemon";
      PATH = "/nix/var/nix/profiles/default/bin:/nix/var/nix/profiles/default/sbin:/bin:/sbin:/usr/bin:/usr/sbin";
      NIX_SSL_CERT_FILE = "/nix/var/nix/profiles/default/etc/ssl/certs/ca-bundle.crt";
    };
    maximumTimeout = 60 * 60 * 3;
    tagList = [ "nix" ];
  };

  # runner for building in docker via host's nix-daemon
  # nix store will be readable in runner, might be insecure
  # runner for building docker images
  genDockerRunner = { registrationConfigFile }: {
    inherit registrationConfigFile;
    dockerImage = "docker:stable";
    dockerVolumes = [
      "/var/run/docker.sock:/var/run/docker.sock"
    ];
    tagList = [ "docker-images" ];
  };

  # runner for executing stuff on host system (very insecure!)
  # make sure to add required packages (including git!)
  # to `environment.systemPackages`
  genShellRunner = { registrationConfigFile }: {
    inherit registrationConfigFile;
    executor = "shell";
    tagList = [ "shell" ];
  };

  # runner for everything else
  genDefaultRunner = { registrationConfigFile }: {
    inherit registrationConfigFile;
    dockerImage = "debian:stable";
  };
in
mkIf ((elem "gitlab-runner" cfg.services) && cfg.gitlab-runner.sopsFile != null) {
  virtualisation.docker.enable = true;

  sops.secrets = (listToAttrs (map
    ({ tag, repo }: nameValuePair (getSopsKey repo tag) {
      inherit (cfg.gitlab-runner) sopsFile;
      key = "gitlab-runner/${tag}/${repo}";
      mode = "0400";
    })
    flattenNames
  )) // {
    no_scope_github_token = {
      key = "no_scope_github_token";
      mode = "0400";
    };
  };

  services.gitlab-runner = {
    enable = true;
    concurrent = 10;

    services = listToAttrs (
      map
        ({ tag, repo }:
          let
            serviceName = getServiceName repo tag;
            sopsKey = getSopsKey repo tag;
          in
          nameValuePair serviceName (
            if (tag == "nix") then
            # File should contain at least these two variables:
            # `CI_SERVER_URL`
            # `REGISTRATION_TOKEN`
              genNixRunner { registrationConfigFile = config.sops.secrets.${sopsKey}.path; }
            else if (tag == "docker") then
              genDockerRunner { registrationConfigFile = config.sops.secrets.${sopsKey}.path; }
            else if (tag == "shell") then
              genShellRunner { registrationConfigFile = config.sops.secrets.${sopsKey}.path; }
            else
              genDefaultRunner { registrationConfigFile = config.sops.secrets.${sopsKey}.path; }
          ))
        flattenNames
    );
  };
}
