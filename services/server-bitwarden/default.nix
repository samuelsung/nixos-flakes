{ pkgs, config, lib, ... }:

with lib;
let
  cfg = config.systemCustomize;
in
mkIf (elem "server-bitwarden" cfg.services) {
  networking.firewall.allowedTCPPorts = [ cfg.server-bitwarden.port ];

  services.vaultwarden = {
    enable = true;
    config = {
      rocketPort = cfg.server-bitwarden.port;
      signupsAllowed = false;
    };
  };
}
