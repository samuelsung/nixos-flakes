{ config, lib, pkgs, ... }:

with lib;
let
  cfg = config.systemCustomize;

  inherit (cfg.server-restic)
    sopsFile
    port;
in
mkIf (elem "server-restic" cfg.services) {
  # NOTICE(samuelsung):
  # to generate htpasswd: htpasswd -B -c .htpasswd username (-B/-s is required)
  # to modify htpasswd: htpasswd -B .htpasswd username
  sops.secrets."restic-htpasswd" = {
    inherit sopsFile;
    format = "yaml";
    owner = config.users.users.restic.name;
    mode = "0400";
    key = "htpasswd";
    path = config.services.restic.server.dataDir + "/.htpasswd";
  };

  environment.systemPackages = with pkgs; [
    apacheHttpd
  ];

  services.restic.server = {
    enable = true;
    privateRepos = true;
    dataDir = "/srv/restic-server";
    listenAddress = ":${builtins.toString port}";
  };

  networking.firewall = {
    allowedTCPPorts = [ port ];
  };
}
