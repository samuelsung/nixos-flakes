# NOTE(samuelsung): to generate hmac secret, run:
# openvpn --genkey secret <name> NOTE(samuelsung): to verify the cert is signed by the given ca, run:
# openssl verify -CAfile <ca.crt> <client/srever.crt>

{ pkgs, config, lib, ... }:

with lib;
let
  cfg = config.systemCustomize;

  inherit (cfg.server-openvpn)
    ext-dev
    vpn-dev
    port
    internalIp
    internalMask

    caCertSopsFile
    certSopsFile
    keySopsFile
    hmacSopsFile
    dhSopsFile

    withPamAuth;

  gen-openvpn-config = pkgs.writeShellScriptBin "gen-openvpn-config" ''
    if [ "$(whoami)" != root ]; then
      echo "Please run this script as root" > /dev/stderr
      exit 1
    fi

    currentIp=$(ip address show "${ext-dev}" | grep "inet" | head --lines 1 | awk '{print $2}' | cut -f1 -d'/');

    cat <<EOL
    client

    dev tun
    proto udp
    remote ''${currentIp} ${toString port}

    <ca>
    $(cat ${config.sops.secrets."openvpn-ca-cert".path})
    </ca>
    cert client.crt
    key client.key

    cipher AES-256-CBC
    auth SHA512
    # TLS Security
    <tls-crypt>
    $(cat ${config.sops.secrets."openvpn-hmac".path})
    </tls-crypt>
    tls-version-min 1.2

    keepalive 20 60

    remote-cert-tls server

    resolv-retry infinite

    nobind

    persist-tun
    persist-key

    # Redirect all Connection through OpenVPN Server
    redirect-gateway def1

    mute-replay-warnings
    verb 3
  '';
in
mkIf (elem "server-openvpn" cfg.services) {
  sops.secrets = {
    "openvpn-ca-cert" = {
      sopsFile = caCertSopsFile;
      format = "binary";
      mode = "0400";
    };
    "openvpn-server-cert" = {
      sopsFile = certSopsFile;
      format = "binary";
      mode = "0400";
    };
    "openvpn-server-key" = {
      sopsFile = keySopsFile;
      format = "binary";
      mode = "0400";
    };
    "openvpn-hmac" = {
      sopsFile = hmacSopsFile;
      format = "binary";
      mode = "0400";
    };
    "openvpn-dh" = {
      sopsFile = dhSopsFile;
      format = "binary";
      mode = "0400";
    };
  };

  # sudo systemctl start nat
  networking.nat = {
    enable = true;
    externalInterface = ext-dev;
    internalInterfaces = [ vpn-dev ];
  };
  networking.firewall.trustedInterfaces = [ vpn-dev ];
  networking.firewall.allowedUDPPorts = [ port ];

  environment.systemPackages = [
    pkgs.openvpn
    pkgs.easyrsa
    pkgs.openssl
    gen-openvpn-config
  ]; # for key generation

  services.openvpn.servers.openvpn-server.config = ''
    # OpenVPN Port, Protocol, and the Tun
    port ${toString port}
    proto udp
    dev ${vpn-dev}

    # Network Configuration - Internal network
    server ${internalIp} ${internalMask}

    persist-tun
    persist-key

    # DNS
    push "dhcp-option DNS 1.1.1.1"
    push "dhcp-option DNS 1.0.0.1"

    # Enable multiple clients to connect with the same certificate key
    duplicate-cn

    # OpenVPN Server Certificate - CA, server key and certificate
    ca ${config.sops.secrets."openvpn-ca-cert".path}
    cert ${config.sops.secrets."openvpn-server-cert".path}
    key ${config.sops.secrets."openvpn-server-key".path}
    dh ${config.sops.secrets."openvpn-dh".path}

    auth SHA512
    # TLS Security
    tls-server
    tls-crypt ${config.sops.secrets."openvpn-hmac".path}
    tls-version-min 1.2

    # Data encryption cipher
    cipher AES-256-CBC

    # Don't negotiate ciphers
    ncp-disable

    # Other Configuration
    keepalive 20 60

    # Require client certificates to have the correct extended key usage
    remote-cert-tls client

    # Reject connections without certificates
    verify-client-cert require

    ${if withPamAuth then ''
      plugin ${pkgs.openvpn}/lib/openvpn/plugins/openvpn-plugin-auth-pam.so login
    '' else ""}

    # Drop privilege
    user ${config.users.users.nobody.name}
    group ${config.users.groups.nogroup.name}

    # OpenVPN Log
    log-append /var/log/openvpn.log
    verb 3
  '';
}
