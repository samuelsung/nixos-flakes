{ config, lib, ... }:

with lib;
let
  cfg = config.systemCustomize;
in
mkIf (elem "qemu-guest-agent" cfg.services) {
  services.qemuGuest.enable = true;
}
