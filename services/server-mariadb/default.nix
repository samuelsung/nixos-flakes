{ config, lib, pkgs, ... }:

with lib;
let
  cfg = config.systemCustomize;
in
mkIf (elem "server-mariadb" cfg.services) {
  services.mysql = {
    enable = true;
    package = pkgs.mariadb;
  };
}
