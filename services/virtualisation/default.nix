{ config, lib, pkgs, ... }:

with lib;
let
  cfg = config.systemCustomize;
in
mkMerge [
  (mkIf (elem "virtualisation" cfg.services) {
    virtualisation.libvirtd.enable = true;
    programs.dconf.enable = true;
  })

  (mkIf ((elem "virtualisation" cfg.services) && cfg.virtualisation.iscsi) {
    nixpkgs.config.packageOverrides = pkgs: {
      libvirt = pkgs.libvirt.override { enableIscsi = true; };
    };

    environment.systemPackages = with pkgs; [
      openiscsi
    ];

    systemd.services.iscsid = {
      description = "iSCSI daemon";
      path = [ pkgs.openiscsi ];
      script = "iscsid -f";
      wantedBy = [ "multi-user.target" ];
    };
  })

  (mkIf ((elem "virtualisation" cfg.services) && cfg.virtualisation.looking-glass) {
    environment.systemPackages = with pkgs; [
      (scream.override {
        pulseSupport = true;
      })

      unstable.looking-glass-client
    ];

    networking.firewall.allowedUDPPorts = [
      4010 ## for scream audio
    ];
  })
]
