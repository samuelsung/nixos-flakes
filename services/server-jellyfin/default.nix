{ config, lib, ... }:

with lib;
let
  cfg = config.systemCustomize;
in
mkIf (elem "server-jellyfin" cfg.services) {
  services.jellyfin = {
    enable = true;
  };

  networking.firewall = {
    allowedTCPPorts = [ 8096 ];
  };
}
