{
  description = "personal NixOS configurations with flakes";

  inputs = {
    dmenu.url = "gitlab:samuelsung/dmenu";
    dmenu.inputs.nixpkgs.follows = "nixpkgs";

    dwm.url = "gitlab:samuelsung/dwm";
    dwm.inputs.nixpkgs.follows = "nixpkgs";

    final-fantasy-xiv-sync.url = "gitlab:samuelsung/final-fantasy-xiv-sync";

    flake-utils.url = "github:numtide/flake-utils";

    home-manager.url = "github:nix-community/home-manager/release-22.05";
    home-manager.inputs.nixpkgs.follows = "nixpkgs";

    music-playlist.url = "gitlab:samuelsung/music-playlist";
    music-playlist.flake = false;

    neovim-flake.url = "gitlab:samuelsung/neovim-flake";
    neovim-flake.inputs.nixpkgs.follows = "nixpkgs";

    nixos-generators.url = "github:nix-community/nixos-generators";
    nixos-generators.inputs.nixpkgs.follows = "nixpkgs";

    nixos-hardware.url = "github:NixOS/nixos-hardware/master";

    nixpkgs.url = "nixpkgs/nixos-22.05";
    nixpkgs-master.url = "nixpkgs/master";
    nixpkgs-unstable.url = "nixpkgs/nixos-unstable";

    nur.url = "github:nix-community/NUR";

    scripts.url = "gitlab:samuelsung/scripts";

    slock.url = "gitlab:samuelsung/slock";
    slock.inputs.nixpkgs.follows = "nixpkgs";

    sops-nix.url = "github:Mic92/sops-nix";

    st.url = "gitlab:samuelsung/st";
    st.inputs.nixpkgs.follows = "nixpkgs";
  };

  outputs = args: import ./outputs.nix args;
}
