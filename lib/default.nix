{ custom-package-overlay
, home-manager
, nixosGenerate
, nixosSystem
, nixpkgs
, nixpkgs-alt-overlay
, nur
, sops-nix
}:

{
  mkSystem = import ./mkSystem {
    inherit
      custom-package-overlay
      home-manager
      nixosGenerate
      nixosSystem
      nixpkgs
      nixpkgs-alt-overlay
      nur
      sops-nix;
  };
}
