{ type # desktop # server # iso
, services ? [ ]
, backlightCtl ? false
, users
, resticClientSopsFile ? null
, ff14MergeStrategies ? "theirs"
, ff14AspectRatio ? "16:9"
, timeServers ? null

, server-bitwarden ? null
, server-openvpn ? null
, server-restic ? null
, server-reverseproxy ? null

, gitlab-runner ? null
, virtualisation ? null
, firewall ? null
, ...
}:

{ lib, ... }:

with lib;

let
  setDefault = org: def: recursiveUpdate def (if org == null then { } else org);

  serverBitwardenWithDefaults = setDefault server-bitwarden {
    port = 8000;
  };

  serverOpenvpnWithDefaults = setDefault server-openvpn {
    ext-dev = null;
    vpn-dev = "tun0";
    port = 1194;
    internalIp = "10.5.0.0";
    internalMask = "255.255.255.0";

    caCertSopsFile = null;
    certSopsFile = null;
    keySopsFile = null;
    hmacSopsFile = null;
    dhSopsFile = null;

    withPamAuth = false;
  };

  serverResticWithDefaults = setDefault server-restic {
    sopsFile = null;
    port = 8000;
  };

  serverReverseproxyWithDefaults = setDefault server-reverseproxy {
    acmeEmail = null;
    targets = { };
  };

  gitlabRunnerWithDefaults = setDefault gitlab-runner {
    sopsFile = null;
  };

  timeServersWithDefaults = setDefault timeServers {
    servers = [ ];
    acceptHighRootDistanceTimeServers = false;
  };

  virtualisationWithDefaults = setDefault virtualisation {
    iscsi = false;
    looking-glass = false;
  };

  firewallWithDefaults = setDefault firewall {
    allowedTCPPorts = [ ];
    allowedTCPPortRanges = [ ];
    allowedUDPPorts = [ ];
    allowedUDPPortRanges = [ ];
  };
in
{
  options.systemCustomize = {
    type = mkOption {
      type = types.enum [ "desktop" "server" "iso" ];
      default = type;
      readOnly = true;
      description = ''
        the usage type of the system.
      '';
    };

    services = mkOption {
      type = with types; listOf (enum [
        "games"
        "easyrsa"
        "gitlab-runner"
        "qemu-guest-agent"
        "server-bitwarden"
        "server-jellyfin"
        "server-mariadb"
        "server-openvpn"
        "server-restic"
        "server-reverseproxy"
        "virtualisation"
      ]);
      default = services;
      readOnly = true;
      description = ''
        the service(s) that need to be installed on the given system
      '';
    };

    backlightCtl = mkOption {
      type = types.bool;
      default = backlightCtl;
      readOnly = true;
      description = ''
        wether the system needs monitor backlight control.
      '';
    };

    users = mkOption {
      type = with types; listOf (enum [ "samuelsung" "samuelsung_play" ]);
      default = users;
      readOnly = true;
      description = ''
        the users of the system.
      '';
    };

    resticClientSopsFile = mkOption {
      type = with types; nullOr path;
      default = resticClientSopsFile;
      readOnly = true;
      description = ''
        the sops file defined the restic backup locations of the system.
      '';
    };

    ff14MergeStrategies = mkOption {
      type = types.enum [ "ours" "theirs" ];
      default = ff14MergeStrategies;
      readOnly = true;
      description = ''
        the merge strategies of ff14's config
      '';
    };

    ff14AspectRatio = mkOption {
      type = types.enum [ "16:9" "48:9" ];
      default = ff14AspectRatio;
      readOnly = true;
      description = ''
        the aspect ratio of ff14
      '';
    };

    server-bitwarden = {
      port = mkOption {
        type = types.int;
        default = serverBitwardenWithDefaults.port;
        readOnly = true;
        description = ''
          the port of the bitwarden server using.
        '';
      };
    };

    server-openvpn = {
      ext-dev = mkOption {
        type = types.str;
        default = serverOpenvpnWithDefaults.ext-dev;
        readOnly = true;
        description = ''
          the external device for the openvpn server.
        '';
      };

      vpn-dev = mkOption {
        type = types.str;
        default = serverOpenvpnWithDefaults.vpn-dev;
        readOnly = true;
        description = ''
          the internal device for the openvpn server.
        '';
      };

      port = mkOption {
        type = types.int;
        default = serverOpenvpnWithDefaults.port;
        readOnly = true;
        description = ''
          the port for the openvpn server.
        '';
      };

      internalIp = mkOption {
        type = types.str;
        default = serverOpenvpnWithDefaults.internalIp;
        readOnly = true;
        description = ''
          the internal ip for the openvpn server.
        '';
      };

      internalMask = mkOption {
        type = types.str;
        default = serverOpenvpnWithDefaults.internalMask;
        readOnly = true;
        description = ''
          the netmask for internal network for the openvpn server.
        '';
      };

      caCertSopsFile = mkOption {
        type = types.path;
        default = serverOpenvpnWithDefaults.caCertSopsFile;
        readOnly = true;
        description = ''
          the sops file of the ca cert for the openvpn server.
        '';
      };

      certSopsFile = mkOption {
        type = types.path;
        default = serverOpenvpnWithDefaults.certSopsFile;
        readOnly = true;
        description = ''
          the sops file of the cert for the openvpn server.
        '';
      };

      keySopsFile = mkOption {
        type = types.path;
        default = serverOpenvpnWithDefaults.keySopsFile;
        readOnly = true;
        description = ''
          the sops file of the key for the openvpn server.
        '';
      };

      hmacSopsFile = mkOption {
        type = types.path;
        default = serverOpenvpnWithDefaults.hmacSopsFile;
        readOnly = true;
        description = ''
          the sops file of the hmac for the openvpn server.
        '';
      };

      dhSopsFile = mkOption {
        type = types.path;
        default = serverOpenvpnWithDefaults.dhSopsFile;
        readOnly = true;
        description = ''
          the sops file of the dh for the openvpn server.
        '';
      };

      withPamAuth = mkOption {
        type = types.bool;
        default = serverOpenvpnWithDefaults.withPamAuth;
        readOnly = true;
        description = ''
          whether enable pam auth for the openvpn server.
        '';
      };
    };

    server-restic = {
      sopsFile = mkOption {
        type = types.path;
        default = serverResticWithDefaults.sopsFile;
        readOnly = true;
        description = ''
          the sops file for restic server.
        '';
      };

      port = mkOption {
        type = types.int;
        default = serverResticWithDefaults.port;
        readOnly = true;
        description = ''
          the port for the restic server.
        '';
      };
    };

    server-reverseproxy = {
      acmeEmail = mkOption {
        type = types.str;
        default = serverReverseproxyWithDefaults.acmeEmail;
        readOnly = true;
        description = ''
          the acme email for ssl.
        '';
      };

      targets = mkOption {
        type = with types; attrsOf str;
        default = serverReverseproxyWithDefaults.targets;
        readOnly = true;
        description = ''
          the mapping of the reverseproxy
        '';
      };
    };

    gitlab-runner = {
      sopsFile = mkOption {
        type = types.path;
        default = gitlabRunnerWithDefaults.sopsFile;
        readOnly = true;
        description = ''
          the port of the bitwarden server using.
        '';
      };
    };

    timeServers = {
      servers = mkOption {
        type = with types; listOf str;
        default = timeServersWithDefaults.servers;
        readOnly = true;
        description = ''
          servers for ntp
        '';
      };

      acceptHighRootDistanceTimeServers = mkOption {
        type = types.bool;
        default = timeServersWithDefaults.acceptHighRootDistanceTimeServers;
        readOnly = true;
        description = ''
          whether accept time serevers with high root distance as sources.
        '';
      };
    };

    virtualisation = {
      iscsi = mkOption {
        type = types.bool;
        default = virtualisationWithDefaults.iscsi;
        readOnly = true;
        description = ''
          enable iscsi support for libvirtd
        '';
      };

      looking-glass = mkOption {
        type = types.bool;
        default = virtualisationWithDefaults.looking-glass;
        readOnly = true;
        description = ''
          enable looking glass
        '';
      };
    };

    firewall = {
      allowedTCPPorts = mkOption {
        type = types.listOf types.port;
        default = firewallWithDefaults.allowedTCPPorts;
        readOnly = true;
        description =
          ''
            List of TCP ports on which incoming connections are
            accepted.
          '';
      };

      allowedTCPPortRanges = mkOption {
        type = types.listOf (types.attrsOf types.port);
        default = firewallWithDefaults.allowedTCPPortRanges;
        readOnly = true;
        description =
          ''
            A range of TCP ports on which incoming connections are
            accepted.
          '';
      };

      allowedUDPPorts = mkOption {
        type = types.listOf types.port;
        default = firewallWithDefaults.allowedUDPPorts;
        readOnly = true;
        description =
          ''
            List of open UDP ports.
          '';
      };

      allowedUDPPortRanges = mkOption {
        type = types.listOf (types.attrsOf types.port);
        default = firewallWithDefaults.allowedUDPPortRanges;
        readOnly = true;
        description =
          ''
            Range of open UDP ports.
          '';
      };
    };
  };
}
