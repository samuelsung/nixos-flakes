# here is some system info that directly passed to mkSystem
{ hostName
, stateVersion
, timeZone ? "Asia/Hong_Kong"
, locale ? "en_US.UTF-8"
, ...
}: {
  # Time Zone
  time.timeZone = timeZone;

  # Locale
  i18n.defaultLocale = locale;

  # hostname
  networking.hostName = hostName;

  # stateVersion
  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = stateVersion; # Did you read the comment?
}
