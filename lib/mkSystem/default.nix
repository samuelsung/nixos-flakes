{ custom-package-overlay
, home-manager
, nixosGenerate
, nixosSystem
, nixpkgs
, nixpkgs-alt-overlay
, nur
, sops-nix
}:

{ system ? "x86_64-linux"
, extraModules ? [ ]

  # the rest should be options applied to systemCustomize, and configs
, type
, ...
}@args:
let
  configs = import ./configs.nix;
  options = import ./options.nix;
  modules = import ../../modules;

  overlays = [
    custom-package-overlay
    nixpkgs-alt-overlay
    nur.overlay
  ];

  imageGenerator = { modules }: nixosGenerate {
    inherit modules;
    format = type;
    pkgs = import nixpkgs {
      inherit system overlays;
      config = { allowUnfree = true; };
    };
  };

  systemGenerator = { modules }: nixosSystem {
    inherit system;
    modules = modules ++ [
      {
        nixpkgs.overlays = overlays;
        nixpkgs.config.allowUnfree = true;
      }
    ];
  };

  generator =
    if type == "iso"
    then imageGenerator
    else systemGenerator;
in
generator {
  modules = extraModules ++ [
    # load the systemCustomize module
    (options args)
    # apply configs
    (configs args)
    # apply options on home-managers' users
    { home-manager.sharedModules = [ (options args) ]; }
    # import home-manager module
    home-manager.nixosModules.home-manager
    # import sops
    sops-nix.nixosModules.sops

    (import ../../services)
    (import ../../systems)
    (import ../../users)
  ];
}
