{ ... }:

{
  services.facl = {
    enable = true;

    folders = {
      "/srv/repos" = {
        group = "samuelsung_common";
        applyRecursively = true;

        sgid = {
          enableOnFiles = false;
          enableOnDirectories = true;
        };

        acls = {
          user = {
            all = [ "r" "w" "X" ];
          };

          group = {
            all = [ "r" "w" "X" ];
            specific = {
              "samuelsung_common" = [ "r" "w" "X" ];
            };
          };
        };

        defaultAcls = {
          user = {
            all = [ "r" "w" "X" ];
          };

          group = {
            all = [ "r" "w" "X" ];
            specific = {
              "samuelsung_common" = [ "r" "w" "X" ];
            };
          };
        };
      };

      "/srv/music" = {
        group = "samuelsung_common";
        applyRecursively = true;

        sgid = {
          enableOnFiles = false;
          enableOnDirectories = true;
        };

        acls = {
          user = {
            all = [ "r" "w" "X" ];
          };

          group = {
            all = [ "r" "w" "X" ];
            specific = {
              "samuelsung_common" = [ "r" "w" "X" ];
            };
          };
        };

        defaultAcls = {
          user = {
            all = [ "r" "w" "X" ];
          };

          group = {
            all = [ "r" "w" "X" ];
            specific = {
              "samuelsung_common" = [ "r" "w" "X" ];
            };
          };
        };
      };
    };
  };
}
