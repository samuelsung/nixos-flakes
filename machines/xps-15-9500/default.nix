{ ... }:

{
  imports = [
    ./boot.nix
    ./facl.nix
    ./file-system.nix
    ./graphics.nix
    ./link.nix
  ];
}
