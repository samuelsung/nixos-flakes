{ ... }:

{
  fileSystems."/" = {
    device = "/dev/disk/by-uuid/8f1d2313-0be1-4e2c-8675-e83494a58fef";
    fsType = "ext4";
    options = [ "acl" ];
  };

  fileSystems."/boot" = {
    device = "/dev/disk/by-uuid/A727-AEF1";
    fsType = "vfat";
  };

  # fileSystems."/home/samuelsung/vault" = {
  #   device = "//10.89.90.20/Main_Set";
  #   fsType = "cifs";
  #   options = let
  #     # this line prevents hanging on network split
  #     # automount_opts = "x-systemd.automount,noauto,x-systemd.idle-timeout=60,x-systemd.device-timeout=5s,x-systemd.mount-timeout=5s";
  #     automount_opts = "uid=33,gid=33,rw,nounix,iocharset=utf8,file_mode=0777,dir_mode=0777";
  #   in ["${automount_opts},credentials=/etc/nixos/secrets/smb-vault-secrets"]; # Remember to change the secret to 600 and root
  # };

  swapDevices = [ ];
}
