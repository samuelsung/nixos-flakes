{ mkSystem
, nixos-hardware
}:

let
  common = import ./common;
in
{
  nixosConfigurations.tr3960x = mkSystem {
    type = "desktop";
    services = [ "games" "virtualisation" ];
    hostName = "tr3960x";
    users = [ "samuelsung" "samuelsung_play" ];
    stateVersion = "20.09";

    firewall.allowedTCPPorts = [ 3000 ];

    ff14AspectRatio = "48:9";
    ff14MergeStrategies = "ours";

    resticClientSopsFile = ../machines/3960x/restic-client.tr3960x-host-secret.json;

    virtualisation.iscsi = true;
    virtualisation.looking-glass = true;

    extraModules = [ (import ./3960x) ]
      ++ (with common; [
      samba
      wireguard
    ]);
  };

  nixosConfigurations.blade-stealth = mkSystem {
    type = "desktop";
    services = [ "virtualisation" ];
    hostName = "blade-stealth";
    users = [ "samuelsung" "samuelsung_play" ];
    stateVersion = "21.05";

    firewall.allowedTCPPorts = [ 3000 ];

    backlightCtl = true;

    extraModules = [ (import ./blade-stealth) ]
      ++ (with common; [
      samba
      wireguard
    ]);
  };

  nixosConfigurations.xps-15-9500 = mkSystem {
    type = "desktop";
    services = [ "games" "virtualisation" ];
    hostName = "xps-15-9500";
    users = [ "samuelsung" "samuelsung_play" ];
    stateVersion = "21.05";

    firewall.allowedTCPPorts = [ 3000 ];

    ff14AspectRatio = "16:9";
    ff14MergeStrategies = "theirs";

    backlightCtl = true;

    resticClientSopsFile = ../machines/xps-15-9500/restic-client.xps-15-9500-host-secret.json;

    extraModules = [
      (import ./xps-15-9500)
      nixos-hardware.nixosModules.dell-xps-15-9500-nvidia
    ] ++ (with common; [
      samba
      wireguard
    ]);
  };

  nixosConfigurations.garemarudo = mkSystem {
    type = "desktop";
    hostName = "garemarudo";
    users = [ "samuelsung" ];
    stateVersion = "20.09";

    timeServers = {
      servers = [ "ifsdc04.ifshk.com" ];
      acceptHighRootDistanceTimeServers = true;
    };

    firewall.allowedTCPPorts = [ 8080 9000 ];

    extraModules = [ (import ./garemarudo) ]
      ++ (with common; [
      opengl-intel
      samba
      wireguard
    ]);
  };

  nixosConfigurations.gitlab-runner = mkSystem {
    type = "server";
    services = [ "qemu-guest-agent" "gitlab-runner" ];
    hostName = "gitlab-runner";
    users = [ "samuelsung" ];
    stateVersion = "20.09";

    gitlab-runner.sopsFile = ./gitlab-runner/gitlab.com-reg.gitlab-runner-host-secret.json;

    extraModules = [ (import ./gitlab-runner) ];
  };

  nixosConfigurations.reverseproxy = mkSystem {
    type = "server";
    services = [ "qemu-guest-agent" "server-reverseproxy" ];
    hostName = "reverseproxy";
    users = [ "samuelsung" ];
    stateVersion = "20.09";

    server-reverseproxy = {
      acmeEmail = "sunghoyin@samuelsung1998.net";
      targets = {
        "bitwarden.samuelsung1998.net" = "http://10.89.90.26:8000";
        "jellyfin.samuelsung1998.net" = "http://10.89.90.28:8096";
        "nextcloud.samuelsung1998.net" = "http://10.89.90.21:80";
      };
    };

    extraModules = [ (import ./reverseproxy) ];
  };

  nixosConfigurations.jellyfin = mkSystem {
    type = "server";
    services = [ "qemu-guest-agent" "server-jellyfin" ];
    hostName = "jellyfin";
    users = [ "samuelsung" ];
    stateVersion = "20.09";

    extraModules = [ (import ./jellyfin) ];
  };

  nixosConfigurations.bitwarden = mkSystem {
    type = "server";
    services = [ "qemu-guest-agent" "server-bitwarden" ];
    hostName = "bitwarden";
    users = [ "samuelsung" ];
    stateVersion = "20.09";

    server-bitwarden.port = 8000;

    extraModules = [ (import ./bitwarden) ];
  };

  nixosConfigurations.tataru = mkSystem {
    type = "server";
    services = [ "qemu-guest-agent" "server-restic" "virtualisation" ];
    hostName = "tataru";
    users = [ "samuelsung" ];
    stateVersion = "21.11";

    server-restic.sopsFile = ../machines/tataru/restic-server.tataru-host-secret.yml;

    extraModules = [ (import ./tataru) ];
  };

  nixosConfigurations.arufino = mkSystem {
    type = "server";
    services = [ "qemu-guest-agent" "server-openvpn" ];
    hostName = "arufino";
    users = [ "samuelsung" ];
    stateVersion = "21.11";
    timeZone = "Asia/Tokyo";

    server-openvpn = {
      ext-dev = "enp1s0";
      port = 3939;

      internalIp = "10.89.92.0";
      internalMask = "255.255.255.0";

      caCertSopsFile = ./arufino/ca.crt.arufino-host-secret;
      certSopsFile = ./arufino/arufino-server.crt.arufino-host-secret;
      keySopsFile = ./arufino/arufino-server.key.arufino-host-secret;
      hmacSopsFile = ./arufino/hmac.arufino-host-secret;
      dhSopsFile = ./arufino/dh.pem.arufino-host-secret;
    };

    extraModules = [ (import ./arufino) ];
  };

  nixosConfigurations.minfiria = mkSystem {
    type = "server";
    services = [ "qemu-guest-agent" "easyrsa" ];
    hostName = "minfiria";
    users = [ "samuelsung" ];
    stateVersion = "21.11";

    extraModules = [ (import ./minfiria) ];
  };

  nixosImages.x86_64-linux.minimal-iso = mkSystem {
    type = "iso";
    services = [ "qemu-guest-agent" ];
    hostName = "minimal-iso";
    users = [ "samuelsung" ];
    stateVersion = "21.11";

    extraModules = [ (import ./minimal-iso) ];
  };
}
