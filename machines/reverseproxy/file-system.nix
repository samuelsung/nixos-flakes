{ ... }:

{
  fileSystems."/" = {
    device = "/dev/disk/by-uuid/3f341dcd-5f26-44ca-a492-3131d20a36fd";
    fsType = "ext4";
  };

  fileSystems."/boot" = {
    device = "/dev/disk/by-uuid/9CF3-8077";
    fsType = "vfat";
  };

  swapDevices = [
    { device = "/dev/disk/by-uuid/1b8964f2-8fec-4a20-853f-75246f02a627"; }
  ];
}
