{
  firewall = import ./firewall;
  opengl-intel = import ./opengl/intel.nix;
  samba = import ./samba;
  wireguard = import ./wireguard;
}
