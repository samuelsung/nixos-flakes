{ ... }:

{
  imports = [
    ./boot.nix
    ./facl.nix
    ./file-system.nix
    ./link.nix
    ./monitors.nix
  ];
}
