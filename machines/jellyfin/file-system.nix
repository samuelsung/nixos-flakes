{ config, ... }:

{
  fileSystems."/" = {
    device = "/dev/disk/by-uuid/6b56d528-b5d9-4e1c-bd76-78ec58c50b06";
    fsType = "ext4";
  };

  fileSystems."/boot" = {
    device = "/dev/disk/by-uuid/AB65-14F9";
    fsType = "vfat";
  };

  sops.secrets."home_vault_secret" = {
    sopsFile = ../common/secrets/home.home-secret.yaml;
  };

  fileSystems."/mnt/freenas" = {
    device = "//10.89.90.20/Main_Set";
    fsType = "cifs";
    options =
      let
        # this line prevents hanging on network split
        # automount_opts = "x-systemd.automount,noauto,x-systemd.idle-timeout=60,x-systemd.device-timeout=5s,x-systemd.mount-timeout=5s";
        # automount_opts = "uid=33,gid=33,rw,nounix,iocharset=utf8,file_mode=0777,dir_mode=0777";
        automount_opts = "uid=1000,gid=1000,rw,iocharset=utf8,file_mode=0777,dir_mode=0777";
      in
      [ "${automount_opts},credentials=${config.sops.secrets."home_vault_secret".path}" ];
  };
}
