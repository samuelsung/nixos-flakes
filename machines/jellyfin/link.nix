{ ... }:

{
  services.links = {
    enable = true;

    links = {
      "/home/samuelsung_play/repos" = { source = "/srv/repos"; };
      "/home/samuelsung/repos" = { source = "/srv/repos"; };
    };
  };
}
