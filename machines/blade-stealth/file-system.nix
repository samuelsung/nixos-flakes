{ ... }:

{
  fileSystems."/" = {
    device = "/dev/disk/by-uuid/8f6d99ee-fc37-49a9-8dc2-66f50750b328";
    fsType = "ext4";
    options = [ "acl" ];
  };

  fileSystems."/boot" = {
    device = "/dev/disk/by-uuid/D2AD-D135";
    fsType = "vfat";
  };

  swapDevices = [
    { device = "/dev/disk/by-uuid/48fb62c7-d40f-4ff2-a33f-0a45e3d6d8e3"; }
  ];
}
