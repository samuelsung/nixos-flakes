{ ... }:

{
  services.xserver.xrandrHeads = [
    {
      output = "DisplayPort-0";
      primary = false;
      monitorConfig = ''
        Option "Position" "1920 0"
      '';
    }
    {
      output = "DisplayPort-1";
      primary = false;
      monitorConfig = ''
        Option "Position" "3840 0"
      '';
    }
    {
      output = "DisplayPort-2";
      primary = true;
      monitorConfig = ''
        Option "Position" "0 0"
      '';
    }
  ];
}
