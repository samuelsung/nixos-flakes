{ ... }:

{
  hardware.pulseaudio.daemon.config = {
    default-sample-channels = 2;
  };

  # Map front-left,right to anothor audio device.
  hardware.pulseaudio.extraConfig = ''
    load-module module-remap-sink sink_name=Stereo sink_properties="device.description='Stereo'" remix=no master=alsa_output.usb-Generic_USB_Audio-00.HiFi___ucm0005.hw_ALC1220VBDT__sink channels=2 master_channel_map=front-left,front-right channel_map=front-left,front-right
  '';
}
