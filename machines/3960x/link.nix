{ ... }:

{
  services.links = {
    enable = true;

    links = {
      "/srv/music" = { source = "/mnt/vault/library/audio/music"; };
      "/home/samuelsung_play/repos" = { source = "/srv/repos"; };
      "/home/samuelsung/repos" = { source = "/srv/repos"; };
      "/home/samuelsung_play/music" = { source = "/srv/music"; };
      "/home/samuelsung/music" = { source = "/srv/music"; };
    };
  };
}
