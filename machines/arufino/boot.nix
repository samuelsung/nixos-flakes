{ modulesPath, ... }:

{
  imports = [
    (modulesPath + "/profiles/qemu-guest.nix")
  ];

  # Use the GRUB 2 boot loader
  boot.loader.grub.enable = true;
  boot.loader.grub.version = 2;

  boot.initrd.enable = true;
  boot.initrd.availableKernelModules = [
    "ahci"
    "xhci_pci"
    "virtio_pci"
    "sr_mod"
    "virtio_blk"
  ];

  boot.initrd.kernelModules = [ ];
  boot.extraModulePackages = [ ];

  boot.blacklistedKernelModules = [ "pcspkr" ];
}
