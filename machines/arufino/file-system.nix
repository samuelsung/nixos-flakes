{ ... }:

{
  boot.loader.grub.device = "/dev/vda";

  fileSystems."/" = {
    device = "/dev/disk/by-label/ROOT";
    fsType = "ext4";
    options = [ "acl" ];
  };

  swapDevices = [
    { device = "/dev/disk/by-label/SWAP"; }
  ];
}
