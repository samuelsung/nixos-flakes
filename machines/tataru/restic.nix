{ config, ... }:

{
  sops.secrets."tataru-mnt-vault-restic-password" = {
    sopsFile = ./restic-client.tataru-host-secret.json;
    owner = "root";
    key = "password";
    mode = "0400";
  };

  sops.secrets."tataru-mnt-synology" = {
    sopsFile = ../../users/common/system/ssh-keys/keys/secret-keys/tataru/restic-to-restic-synology.tataru-host-secret.json;
    owner = "root";
    mode = "0400";
    key = "private";
  };

  services.restic.backups = {
    remotebackup = {
      paths = [ "/mnt/vault" ];
      repository = "sftp:restic@synology:/restic";
      passwordFile = config.sops.secrets."tataru-mnt-vault-restic-password".path;
      extraOptions = [
        "sftp.command='ssh restic@synology -i ${config.sops.secrets."tataru-mnt-synology".path} -s sftp'"
      ];
      timerConfig = {
        OnCalendar = "00:05";
        RandomizedDelaySec = "5h";
      };
    };
  };
}
