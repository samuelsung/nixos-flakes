{ config, ... }:

{
  fileSystems."/" = {
    device = "/dev/disk/by-label/ROOT";
    fsType = "ext4";
    options = [ "acl" ];
  };

  fileSystems."/boot" = {
    device = "/dev/disk/by-label/BOOT";
    fsType = "vfat";
  };

  sops.secrets."tataru-mnt-vault" = {
    sopsFile = ../../users/common/system/ssh-keys/keys/secret-keys/tataru/restic-to-restic-vault.tataru-host-secret.json;
    owner = "root";
    mode = "0400";
    key = "private";
  };

  fileSystems."/mnt/vault" = {
    device = "restic@vault:/mnt/Main_Pool/Main_Set";
    fsType = "fuse.sshfs";
    options = [
      "ro"
      "idmap=user"
      "uid=${toString config.users.users.root.uid}"
      "gid=${toString config.users.groups.root.gid}"
      "noauto"
      "x-systemd.automount"
      "_netdev"
      "identityfile=${config.sops.secrets."tataru-mnt-vault".path}"
    ];
  };

  swapDevices = [
    { device = "/dev/disk/by-label/SWAP"; }
  ];
}
