{ modulesPath, ... }:

{
  imports = [
    (modulesPath + "/profiles/qemu-guest.nix")
  ];

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  boot.initrd.enable = true;
  boot.initrd.availableKernelModules = [
    "uhci_hcd"
    "ehci_pci"
    "ahci"
    "virtio_pci"
    "sd_mod"
    "sr_mod"
  ];

  boot.initrd.kernelModules = [ "kvm-amd" ];
  boot.extraModulePackages = [ ];

  boot.blacklistedKernelModules = [ "pcspkr" ];
}
