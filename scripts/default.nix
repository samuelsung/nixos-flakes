{ nixpkgs, flake-utils }:

let
  buildScript = { name, buildInputs, file, pkgs }:
    let
      buildInputs = with pkgs; [ git ];
      script = (pkgs.writeScriptBin name (builtins.readFile file)).overrideAttrs (old: {
        # This patch /bin/sh to /nix/store/<hash>-some-bash/bin/sh
        # see https://github.com/NixOS/nixpkgs/blob/master/pkgs/build-support/setup-hooks/patch-shebangs.sh
        buildCommand = "${old.buildCommand}\n patchShebangs $out";
      });
    in
    pkgs.symlinkJoin {
      inherit name;
      paths = [ script ] ++ buildInputs;
      buildInputs = [ pkgs.makeWrapper ];
      postBuild = "wrapProgram $out/bin/${name} --prefix PATH : $out/bin";
    };
in
flake-utils.lib.eachDefaultSystem (system:
  let
    pkgs = nixpkgs.legacyPackages.${system};
    inherit (pkgs) callPackage;
  in
  {
    packages = {
      get_all_script_name = callPackage buildScript {
        file = ./get_all_script_name;
        name = "get_all_script_name";
        buildInputs = with pkgs; [ gnugrep coreutils ];
      };
    };
  }
)
