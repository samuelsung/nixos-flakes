{ pkgs, lib }:

{ toolbar ? [ ]
, menu ? [ ]
}:

with lib;
let
  escapeXML = replaceStrings [ ''"'' "'" "<" ">" "&" ] [
    "&quot;"
    "&apos;"
    "&lt;"
    "&gt;"
    "&amp;"
  ];

  entryMapper = entry:
    ''
      <DT><A HREF="${escapeXML entry.url}" ADD_DATE="0" LAST_MODIFIED="0"${
        lib.optionalString (entry.keyword != null)
        " SHORTCUTURL=\"${escapeXML entry.keyword}\""
      }>${escapeXML entry.name}</A>
    '';

  folderMapper = { isFolder ? false, name ? "", ... }@entry:
    if isFolder then
      ''
        <DT><H3>${escapeXML name}</H3>
        <DL><p>
          ${concatStrings (builtins.map folderMapper entry.children)}
        </DL><p>
      ''
    else entryMapper entry;

  bookmarksEntries = builtins.map folderMapper;

in
pkgs.writeText "firefox-bookmarks.html" ''
  <!DOCTYPE NETSCAPE-Bookmark-file-1>
  <!-- This is an automatically generated file.
    It will be read and overwritten.
    DO NOT EDIT! -->
  <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=UTF-8">
  <TITLE>Bookmarks</TITLE>
  <H1>Bookmarks Menu</H1>
  <DL><p>
    ${concatStrings (bookmarksEntries menu)}
    <DT><H3 ADD_DATE="0" LAST_MODIFIED="0" PERSONAL_TOOLBAR_FOLDER="true">Bookmarks Toolbar</H3>
    <DL><p>
      ${concatStrings (bookmarksEntries toolbar)}
    </DL><p>
  </DL>
''
