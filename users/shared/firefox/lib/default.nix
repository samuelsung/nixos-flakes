{ pkgs, lib }:

{
  mapLockPref = import ./mapLockPref { inherit pkgs lib; };
  mkBookmarkFile = import ./mkBookmarkFile { inherit pkgs lib; };
  mkSearchFile = import ./mkSearchFile { inherit pkgs lib; };
}
