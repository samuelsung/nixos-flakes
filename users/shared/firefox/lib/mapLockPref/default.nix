{ pkgs, lib }:

with lib;

prefs: concatStrings (mapAttrsToList
  (name: value: ''
    lockPref("${name}", ${builtins.toJSON value});
  '')
  prefs)
