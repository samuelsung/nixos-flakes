{ username }:

{ config, lib, ... }:

let
  inherit (builtins) listToAttrs hasAttr;
  inherit (lib) filter nameValuePair;
  inherit (config.networking) hostName;
  inherit (import ../../../common/system/ssh-keys/utils.nix { inherit config lib; })
    keyMapFromThisMachine
    getAccountMap
    getUnmappedToName
    getToName
    getToPort
    getToAccount;

  keyMapFromThisAccount = filter ({ from, ... }: from.account == username) keyMapFromThisMachine;
in
{
  home-manager.users.${username}.programs.ssh = {
    enable = true;
    matchBlocks = (listToAttrs
      (map
        ({ to, ... }: nameValuePair
          (getToName to)
          ({
            host = if to.type == "host" then to.machine else to.service;
            checkHostIP = true;
            identityFile = "~/.ssh/${getUnmappedToName to}";
            extraOptions.Preferredauthentications = "publickey";
            port = getToPort to;
            user = getToAccount to;
          })
        )
        keyMapFromThisAccount
      ));
  };
}
