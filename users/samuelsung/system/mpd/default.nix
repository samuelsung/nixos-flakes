{ username }:

{ pkgs, config, lib, ... }:

with lib;
let
  cfg = config.systemCustomize;
  inherit (pkgs) runCommand;

  move = path: runCommand "move"
    {
      inherit path;
      preferLocalBuild = true;
      allowSubstitutes = false;
      nativeBuildInputs = with pkgs; [ findutils ];
    }
    ''
      mkdir "$out";
      find $path -maxdepth 1 | grep \.m3u8$ | grep -o \.\*\.m3u | while read p; do {
        cp "$p""8" "$out/$(basename $p)"
      }; done;
    '';

  port = 5000 + config.users.users.${username}.uid; # 6000, 6001
in
mkIf (cfg.type == "desktop") {
  home-manager.users.${username} = {
    home.packages = with pkgs; [
      mpc_cli
    ];

    services.mpd.enable = true;
    # Whether to enable MPD, the music player daemon.
    # Type: boolean
    # Default: false
    # Declared by:
    # <home-manager/modules/services/mpd.nix>

    # services.mpd.package
    # The MPD package to run.
    # Type: package
    # Default: "pkgs.mpd"
    # Declared by:
    # <home-manager/modules/services/mpd.nix>

    # services.mpd.dataDir
    # The directory where MPD stores its state, tag cache, playlists etc.
    # Type: path
    # Default: "$XDG_DATA_HOME/mpd"
    # Declared by:
    # <home-manager/modules/services/mpd.nix>

    # services.mpd.dbFile
    # The path to MPD's database. If set to null the parameter is omitted from the configuration.
    # Type: null or string
    # Default: "\${dataDir}/tag_cache"
    # Declared by:
    # <home-manager/modules/services/mpd.nix>

    services.mpd.extraConfig = ''
      auto_update "yes"
      replaygain "auto"
    '';
    # Extra directives added to to the end of MPD's configuration file, mpd.conf. Basic configuration like file location and uid/gid is added automatically to the beginning of the file. For available options see mpd.conf(5).
    # Type: strings concatenated with "\n"
    # Default: ""
    # Declared by:
    # <home-manager/modules/services/mpd.nix>

    # services.mpd.musicDirectory = "/home/samuelsung/music";
    # The directory where mpd reads music from.
    # Type: path or string
    # Default: "$HOME/music"
    # Declared by:
    # <home-manager/modules/services/mpd.nix>

    # services.mpd.network.listenAddress
    # The address for the daemon to listen on. Use any to listen on all addresses.
    # Type: string
    # Default: "127.0.0.1"
    # Example: "any"
    # Declared by:
    # <home-manager/modules/services/mpd.nix>

    services.mpd.network.port = port;
    # The TCP port on which the the daemon will listen.
    # Type: 16 bit unsigned integer; between 0 and 65535 (both inclusive)
    # Default: 6600
    # Declared by:
    # <home-manager/modules/services/mpd.nix>

    # services.mpd.network.startWhenNeeded
    # Enable systemd socket activation.
    # Type: boolean
    # Default: false
    # Declared by:
    # <home-manager/modules/services/mpd.nix>

    services.mpd.playlistDirectory = move pkgs.music-playlist;
    # The directory where mpd stores playlists.
    # Type: path
    # Default: "\${dataDir}/playlists"
    # Declared by:
    # <home-manager/modules/services/mpd.nix>

    programs.ncmpcpp.enable = true;
    # Whether to enable ncmpcpp - an ncurses Music Player Daemon (MPD) client.
    # Type: boolean
    # Default: false
    # Example: true
    # Declared by:
    # <home-manager/modules/programs/ncmpcpp.nix>

    # programs.ncmpcpp.package
    # Package providing the ncmpcpp command.
    # Type: package
    # Default: pkgs.ncmpcpp
    # Example: pkgs.ncmpcpp.override { visualizerSupport = true; }
    # Declared by:
    # <home-manager/modules/programs/ncmpcpp.nix>

    programs.ncmpcpp.bindings = [
      { key = "@"; command = "show_server_info"; }
      { key = "#"; command = "dummy"; } # toggle_bitrate_visibility
      { key = ":"; command = "execute_command"; }
      { key = "mouse"; command = "mouse_event"; }

      { key = "left"; command = "previous_column"; } # master_screen # volume_down
      { key = "right"; command = "next_column"; } # slave_screen # volume_up

      { key = "tab"; command = "slave_screen"; } # next_screen
      { key = "tab"; command = "master_screen"; }

      { key = "shift-tab"; command = "dummy"; } # previous_screen

      { key = "enter"; command = "enter_directory"; }
      { key = "enter"; command = "toggle_output"; }
      { key = "enter"; command = "run_action"; }
      { key = "enter"; command = "play_item"; }

      { key = "space"; command = "add_item_to_playlist"; }
      # { key = "space"; command = "toggle_lyrics_update_on_song_change"; }
      { key = "space"; command = "toggle_visualization_type"; }

      { key = "1"; command = "show_playlist"; }
      { key = "2"; command = "show_browser"; }
      { key = "2"; command = "change_browse_mode"; }
      { key = "3"; command = "show_search_engine"; }
      { key = "3"; command = "reset_search_engine"; }
      { key = "4"; command = "show_media_library"; }
      { key = "4"; command = "toggle_media_library_columns_mode"; }
      { key = "5"; command = "show_playlist_editor"; }
      { key = "6"; command = "show_tag_editor"; }
      { key = "7"; command = "show_outputs"; }
      { key = "8"; command = "show_visualizer"; }

      { key = "+"; command = "dummy"; } # volume_up
      { key = "_"; command = "dummy"; } # volume_down

      { key = "y"; command = "save_tag_changes"; }
      { key = "y"; command = "start_searching"; }

      { key = "Y"; command = "dummy"; } # toggle_replay_gain_mode

      { key = "a"; command = "add_selected_items"; }
      { key = "A"; command = "add"; }

      { key = "I"; command = "show_artist_info"; }

      { key = "u"; command = "update_database"; }

      { key = "U"; command = "dummy"; } # toggle_playing_song_centering

      { key = "d"; command = "delete_playlist_items"; }
      { key = "d"; command = "delete_browser_items"; }
      { key = "d"; command = "delete_stored_playlist"; }

      { key = "delete"; command = "delete_playlist_items"; }
      { key = "delete"; command = "delete_browser_items"; }
      { key = "delete"; command = "delete_stored_playlist"; }

      { key = "n"; command = "scroll_down"; }

      { key = "ctrl-n"; command = "move_sort_order_down"; }
      { key = "ctrl-n"; command = "move_selected_items_down"; }

      { key = "escape"; command = "remove_selection"; }

      { key = "e"; command = "scroll_up"; }

      { key = "ctrl-e"; command = "move_sort_order_up"; }
      { key = "ctrl-e"; command = "move_selected_items_up"; }

      { key = "o"; command = "jump_to_playing_song"; }

      { key = "t"; command = "select_item"; }
      { key = "T"; command = "select_found_items"; }

      { key = "H"; command = "move_selected_items_to"; }

      { key = "M"; command = "dummy"; } # move_selected_items_to

      { key = "v"; command = "select_range"; } # reverse_selection
      { key = "V"; command = "dummy"; } # remove_selection
      { key = "ctrl-v"; command = "dummy"; } # select_range

      { key = "E"; command = "edit_lyrics"; }
      { key = "E"; command = "jump_to_tag_editor"; }
      { key = "E"; command = "jump_to_playlist_editor"; }

      { key = "E"; command = "edit_song"; }
      { key = "E"; command = "edit_library_tag"; }
      { key = "E"; command = "edit_library_album"; }
      { key = "E"; command = "edit_directory_name"; }
      { key = "E"; command = "edit_playlist_name"; }

      { key = "ctrl-b"; command = "jump_to_media_library"; }
      { key = "B"; command = "select_album"; }

      { key = "M"; command = "jump_to_media_library"; }

      { key = "~"; command = "dummy"; } # jump_to_media_library

      { key = "h"; command = "previous_column"; }
      { key = "h"; command = "jump_to_parent_directory"; }
      { key = "h"; command = "replay_song"; }

      { key = "f"; command = "seek_forward"; }

      { key = "b"; command = "seek_backward"; }

      { key = "ctrl-f"; command = "apply_filter"; }

      { key = "F"; command = "dummy"; } # fetch_lyrics_in_background

      { key = "i"; command = "next_column"; }
      { key = "i"; command = "enter_directory"; }
      { key = "i"; command = "play_item"; }

      { key = "k"; command = "next_found_item"; }
      { key = "K"; command = "previous_found_item"; }

      { key = ","; command = "dummy"; }
      { key = "."; command = "dummy"; }

      { key = "g"; command = "move_home"; } # jump_to_position_in_song
      { key = "G"; command = "move_end"; }

      { key = "w"; command = "dummy"; } # toggle_find_mode

      { key = "ctrl-g"; command = "show_song_info"; }

      { key = "ctrl-p"; command = "set_selected_items_priority"; }

      { key = "["; command = "scroll_up_album"; }
      { key = "]"; command = "scroll_down_album"; }

      { key = "{"; command = "scroll_up_artist"; }
      { key = "}"; command = "scroll_down_artist"; }

      { key = "f1"; command = "dummy"; } # show_help
      { key = "ctrl-h"; command = "show_help"; }

      { key = ">"; command = "next"; }
      { key = "<"; command = "previous"; }

      { key = "r"; command = "toggle_repeat"; }
      { key = "l"; command = "show_lyrics"; }

      { key = "L"; command = "dummy"; } # toggle_lyrics_fetcher

      { key = "R"; command = "add_random_items"; }

      { key = "ctrl-r"; command = "refetch_lyrics"; } # reverse_playlist

      { key = "p"; command = "pause"; }
      { key = "P"; command = "stop"; } # toggle_display_mode
      { key = "s"; command = "toggle_single"; }
      { key = "c"; command = "toggle_consume"; }
      { key = "C"; command = "clear_playlist"; }
      { key = "C"; command = "clear_main_playlist"; }
      { key = "z"; command = "toggle_random"; }
      { key = "Z"; command = "shuffle"; }
      { key = "x"; command = "toggle_crossfade"; }
      { key = "X"; command = "set_crossfade"; }

      { key = "ctrl-s"; command = "sort_playlist"; }
      { key = "ctrl-s"; command = "toggle_browser_sort_mode"; }
      { key = "ctrl-s"; command = "toggle_media_library_sort_mode"; }

      { key = "S"; command = "save_playlist"; }

      { key = "ctrl-l"; command = "toggle_screen_lock"; }

      { key = "ctrl-t"; command = "toggle_library_tag_type"; } # toggle_add_mode

      { key = "/"; command = "find"; }
      { key = "/"; command = "find_item_forward"; }
      { key = "?"; command = "find"; }
      { key = "?"; command = "find_item_backward"; }

      { key = "\\\\"; command = "dummy"; } # toggle_interface
      { key = "|"; command = "dummy"; } # toggle_mouse
      { key = "!"; command = "dummy"; } # toggle_separators_between_albums
      { key = "alt-l"; command = "dummy"; } # toggle_fetching_lyrics_in_background
      { key = "="; command = "dummy"; } # show_clock

      { key = "q"; command = "quit"; }
    ];
    # jump_to_position_in_song
    # List of keybindings
    # Type: list of submodules
    # Default: [ ]
    # Example:
    # [
    #   {
    #     key = "j";
    #     command = "scroll_down";
    #   }
    #   { key = "k"; command = "scroll_up"; }
    #   { key = "J"; command = [ "select_item" "scroll_down" ]; }
    #   { key = "K"; command = [ "select_item" "scroll_up" ]; }
    # ]
    # Declared by:
    # <home-manager/modules/programs/ncmpcpp.nix>

    ##### General rules #####
    ##
    ## 1) Because each action has runtime checks whether it's
    ##    ok to run it, a few actions can be bound to one key.
    ##    Actions will be bound in order given in configuration
    ##    file. When a key is pressed, first action in order
    ##    will test itself whether it's possible to run it. If
    ##    test succeeds, action is executed and other actions
    ##    bound to this key are ignored. If it doesn't, next
    ##    action in order tests itself etc.
    ##
    ## 2) It's possible to bind more that one action at once
    ##    to a key. It can be done using the following syntax:
    ##
    ##    def_key "key"
    ##      action1
    ##      action2
    ##      ...
    ##
    ##    This creates a chain of actions. When such chain is
    ##    executed, each action in chain is run until the end of
    ##    chain is reached or one of its actions fails to execute
    ##    due to its requirements not being met. If multiple actions
    ##    and/or chains are bound to the same key, they will be
    ##    consecutively run until one of them gets fully executed.
    ##
    ## 3) When ncmpcpp starts, bindings configuration file is
    ##    parsed and then ncmpcpp provides "missing pieces"
    ##    of default keybindings. If you want to disable some
    ##    bindings, there is a special action called 'dummy'
    ##    for that purpose. Eg. if you want to disable ability
    ##    to crop playlists, you need to put the following
    ##    into configuration file:
    ##
    ##    def_key "C"
    ##      dummy
    ##
    ##    After that ncmpcpp will not bind any default action
    ##    to this key.
    ##
    ## 4) To let you write simple macros, the following special
    ##    actions are provided:
    ##
    ##    - push_character "character" - pushes given special
    ##      character into input queue, so it will be immediately
    ##      picked by ncmpcpp upon next call to readKey function.
    ##      Accepted values: mouse, up, down, page_up, page_down,
    ##      home, end, space, enter, insert, delete, left, right,
    ##      tab, ctrl-a, ctrl-b, ..., ctrl-z, ctrl-[, ctrl-\\,
    ##      ctrl-], ctrl-^, ctrl-_, f1, f2, ..., f12, backspace.
    ##      In addition, most of these names can be prefixed with
    ##      alt-/ctrl-/shift- to be recognized with the appropriate
    ##      modifier key(s).
    ##
    ##    - push_characters "string" - pushes given string into
    ##      input queue.
    ##
    ##    - require_runnable "action" - checks whether given action
    ##      is runnable and fails if it isn't. This is especially
    ##      useful when mixed with previous two functions. Consider
    ##      the following macro definition:
    ##
    ##      def_key "key"
    ##        push_characters "custom_filter"
    ##        apply_filter
    ##
    ##      If apply_filter can't be currently run, we end up with
    ##      sequence of characters in input queue which will be
    ##      treated just as we typed them. This may lead to unexpected
    ##      results (in this case 'c' will most likely clear current
    ##      playlist, 'u' will trigger database update, 's' will stop
    ##      playback etc.). To prevent such thing from happening, we
    ##      need to change above definition to this one:
    ##
    ##      def_key "key"
    ##        require_runnable "apply_filter"
    ##        push_characters "custom_filter"
    ##        apply_filter
    ##
    ##      Here, first we test whether apply_filter can be actually run
    ##      before we stuff characters into input queue, so if condition
    ##      is not met, whole chain is aborted and we're fine.
    ##
    ##    - require_screen "screen" - checks whether given screen is
    ##      currently active. accepted values: browser, clock, help,
    ##      media_library, outputs, playlist, playlist_editor,
    ##      search_engine, tag_editor, visualizer, last_fm, lyrics,
    ##      selected_items_adder, server_info, song_info,
    ##      sort_playlist_dialog, tiny_tag_editor.
    ##
    ##    - run_external_command "command" - runs given command using
    ##      system() function.
    ##
    ## 5) In addition to binding to a key, you can also bind actions
    ##    or chains of actions to a command. If it comes to commands,
    ##    syntax is very similar to defining keys. Here goes example
    ##    definition of a command:
    ##
    ##      def_command "quit" [deferred]
    ##        stop
    ##        quit
    ##
    ##    If you execute the above command (which can be done by
    ##    invoking action execute_command, typing 'quit' and pressing
    ##    enter), ncmpcpp will stop the player and then quit. Note the
    ##    presence of word 'deferred' enclosed in square brackets. It
    ##    tells ncmpcpp to wait for confirmation (ie. pressing enter)
    ##    after you typed quit. Instead of 'deferred', 'immediate'
    ##    could be used. Then ncmpcpp will not wait for confirmation
    ##    (enter) and will execute the command the moment it sees it.
    ##
    ##    Note: while command chains are executed, internal environment
    ##    update (which includes current window refresh and mpd status
    ##    update) is not performed for performance reasons. However, it
    ##    may be desirable to do so in some situration. Therefore it's
    ##    possible to invoke by hand by performing 'update enviroment'
    ##    action.
    ##
    ## Note: There is a difference between:
    ##
    ##         def_key "key"
    ##           action1
    ##
    ##         def_key "key"
    ##           action2
    ##
    ##       and
    ##
    ##         def_key "key"
    ##           action1
    ##           action2
    ##
    ##      First one binds two single actions to the same key whilst
    ##      second one defines a chain of actions. The behavior of
    ##      these two is different and is described in (1) and (2).
    ##
    ## Note: Function def_key accepts non-ascii characters.

    # programs.ncmpcpp.bindings.*.command
    # Command or sequence of commands to be executed
    # Type: string or list of strings
    # Example: "scroll_down"
    # Declared by:
    # <home-manager/modules/programs/ncmpcpp.nix>

    # programs.ncmpcpp.bindings.*.key
    # Key to bind.
    # Type: string
    # Example: "j"
    # Declared by:
    # <home-manager/modules/programs/ncmpcpp.nix>

    # programs.ncmpcpp.mpdMusicDir
    # Value of the mpd_music_dir setting. On Linux platforms the value of services.mpd.musicDirectory is used as the default if services.mpd.enable is true.
    # Type: null or path
    # Default:
    # if pkgs.stdenv.hostPlatform.isLinux && config.services.mpd.enable then config.services.mpd.musicDirectory else null
    # Example: "~/music"
    # Declared by:
    # <home-manager/modules/programs/ncmpcpp.nix>

    programs.ncmpcpp.settings = {
      mpd_port = toString port;

      playlist_disable_highlight_delay = 0;

      # Basics
      external_editor = config.home-manager.users.${username}.home.sessionVariables.EDITOR;
      use_console_editor = "yes";
      system_encoding = "utf-8";
      regular_expressions = "extended";
      allow_for_physical_item_deletion = "no";
      space_add_mode = "always_add";

      ## Song List ##
      # song_columns_list_format = "(10)[blue]{l} (30)[green]{t} (30)[magenta]{a} (30)[yellow]{b}";
      song_list_format = "{$7%a - $9}{$5%t$9}|{$5%f$9}$R{$6%b $9}{$3%l$9}";
      song_columns_list_format = "(20)[blue]{a} (5f)[red]{d} (6f)[green]{NE} (50)[white]{t|f:Title} (20)[cyan]{b} (7f)[magenta]{l}";

      # Layout
      now_playing_prefix = "$u";
      now_playing_suffix = "$/u";

      user_interface = "alternative";

      display_volume_level = "no";
      display_bitrate = "yes";

      # Motion
      cyclic_scrolling = "no";
      autocenter_mode = "no";
      ## Seeking ##
      incremental_seeking = "yes";
      seek_time = "1";

      # Lyrics
      store_lyrics_in_song_dir = "yes";
      follow_now_playing_lyrics = "yes";
      fetch_lyrics_for_current_song_in_background = "yes";
    };
    # Attribute set from name of a setting to its value. For available options see ncmpcpp(1).
    # Type: attribute set of boolean or signed integer or strings
    # Default: { }
    # Example: { ncmpcpp_directory = "~/.local/share/ncmpcpp"; }
    # Declared by:
    # <home-manager/modules/programs/ncmpcpp.nix>
  };
}
