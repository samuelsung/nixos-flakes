{ username }:

{ config, lib, ... }:

with lib;
let
  cfg = config.systemCustomize;
in
mkMerge [
  {
    home-manager.users.${username} = {
      programs.direnv.enable = true;
      programs.direnv.nix-direnv.enable = true;
    };
  }

  # If it is a desktop, stop the derivations being collected by gc
  (mkIf (cfg.type == "desktop") {
    nix.extraOptions = ''
      keep-outputs = true
      keep-derivations = true
    '';
  })
]
