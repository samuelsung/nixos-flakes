{ username }:

{ lib, config, pkgs, ... }@args:

with lib;
let
  cfg = config.systemCustomize;
in
mkMerge (map
  (p: mkIf (elem username cfg.users) (import p { inherit username; } args))
  [
    ./lab
    ./mpd
    ./nix-direnv
    ./ssh
  ]
)
