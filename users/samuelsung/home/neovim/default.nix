{ config, pkgs, ... }:

let
  cfg = config.systemCustomize;
in
{
  home.sessionVariables = {
    EDITOR = "nvim";
    PAGER = "nvimpager";
  };

  home.shellAliases = {
    v = "nvim";
  };

  home.packages = (with pkgs; [
    (if cfg.type == "desktop" then neovim else neovimSlim)
  ]);
}

