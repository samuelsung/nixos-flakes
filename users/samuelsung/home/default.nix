{
  imports = [
    ./dunst
    ./fcitx
    ./firefox
    ./flameshot
    ./git
    ./gtk
    # ./ibus
    ./neofetch
    ./neovim
    ./newsboat
    ./picom
    ./polybar
    ./pulsemixer
    ./ranger
    ./scripts
    ./suckless
    # ./sxhkd
    ./tmux
    ./vifm
    ./vscodium
    ./xdg
    ./xmonad
    ./xorg
    ./zsh
  ];
}
