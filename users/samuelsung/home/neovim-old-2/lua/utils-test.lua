join_tables = function(...)
  local tables = { ... };
  local rtn = {};

  for _, table in ipairs(tables) do
    -- filter all non table
    if (type(table) == 'table') then
      for key, value in pairs(table) do
        rtn[key] = value;
      end;
    end;
  end;

  return rtn;
end

return { join_tables = join_tables }
