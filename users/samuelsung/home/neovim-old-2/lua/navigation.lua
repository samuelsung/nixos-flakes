local autopairs = require('nvim-autopairs');
-- Helper Function
_G.toggle_bool = function(cate, key)
  vim[cate][key] = (not vim[cate][key]);
end
-- Map Leader
vim.g.mapleader = ",";
vim.o.clipboard = 'unnamedplus';

local keys = {
  -- key bindings for colemak
  { from = 'n',          to = 'j',                                                                              mode = { 'n', 'x', 'o' },                                        },
  { from = 'N',          to = 'J',                                                                              mode = { 'n', 'x', 'o' },                                        },
  { from = 'e',          to = 'k',                                                                              mode = { 'n', 'x', 'o' },                                        },
  { from = 'E',          to = 'K',                                                                              mode = { 'n', 'x', 'o' },                                        },
  { from = 'i',          to = 'l',                                                                              mode = { 'n', 'x', 'o' },                                        },
  { from = 'I',          to = 'L',                                                                              mode = { 'n', 'x', 'o' },                                        },
  { from = 'l',          to = 'u',                                                                              mode = { 'n', 'x', 'o' },                                        },
  { from = 'L',          to = 'U',                                                                              mode = { 'n', 'x', 'o' },                                        },
  { from = 'u',          to = 'i',                                                                              mode = { 'n', 'x', 'o' },                                        },
  { from = 'U',          to = 'I',                                                                              mode = { 'n', 'x', 'o' },                                        },
  { from = 'k',          to = 'n',                                                                              mode = { 'n', 'x', 'o' },                                        },
  { from = 'K',          to = 'N',                                                                              mode = { 'n', 'x', 'o' },                                        },

  -- splitting key mappings
  { from = 'sh',         to = '<C-W>h',                                                                         mode = { 'n'           },                                        },
  { from = 'sn',         to = '<C-W>j',                                                                         mode = { 'n'           },                                        },
  { from = 'se',         to = '<C-W>k',                                                                         mode = { 'n'           },                                        },
  { from = 'si',         to = '<C-W>l',                                                                         mode = { 'n'           },                                        },
  { from = 'sH',         to = '<C-W>H',                                                                         mode = { 'n'           },                                        },
  { from = 'sN',         to = '<C-W>J',                                                                         mode = { 'n'           },                                        },
  { from = 'sE',         to = '<C-W>K',                                                                         mode = { 'n'           },                                        },
  { from = 'sI',         to = '<C-W>L',                                                                         mode = { 'n'           },                                        },
  { from = 's=',         to = '<C-W>=',                                                                         mode = { 'n'           },                                        },
  { from = 's.',         to = '<C-W>>',                                                                         mode = { 'n'           },                                        },
  { from = 's,',         to = '<C-W><',                                                                         mode = { 'n'           },                                        },
  { from = 's+',         to = '<C-W>+',                                                                         mode = { 'n'           },                                        },
  { from = 's-',         to = '<C-W>-',                                                                         mode = { 'n'           },                                        },

  -- shortcut to spawn new tab, split
  { from = '<leader>nt', to = ':tab split <CR>',                                                                mode = { 'n'           },                                        },
  { from = '<leader>nv', to = ':vsp <CR>',                                                                      mode = { 'n'           },                                        },
  { from = '<leader>ns', to = ':sp <CR>',                                                                       mode = { 'n'           },                                        },
  { from = 'ZAQ',        to = ':qa!<CR>',                                                                       mode = { 'n'           },                                        },
  { from = 'ZAZ',        to = ':wqa!<CR>',                                                                      mode = { 'n'           },                                        },

  -- Disable ex mode key mapping
  { from = 'Q',          to = '',                                                                               mode = { 'n'           },                                        },

  -- Tab Navigation
  { from = 'th',         to = ':tabprevious<CR>',                                                               mode = { 'n'           },                                        },
  { from = 'ti',         to = ':tabnext<CR>',                                                                   mode = { 'n'           },                                        },

  -- Redo Key Mapping
  { from = 'L',          to = ':redo<CR>',                                                                      mode = { 'n'           },                                        },

  -- Fast Saving Key Mapping
  { from = '<leader>w',  to = ':w!<cr>',                                                                        mode = { 'n'           }, opt = { silent = false }               },

  -- vifm
  { from = '<C-f>f',     to = ':Vifm<CR>',                                                                      mode = { 'n'           },                                        },
  { from = '<C-f>v',     to = ':VsplitVifm<CR>',                                                                mode = { 'n'           },                                        },
  { from = '<C-f>s',     to = ':SplitVifm<CR>',                                                                 mode = { 'n'           },                                        },
  { from = '<C-f>t',     to = ':TabVifm<CR>',                                                                   mode = { 'n'           },                                        },

  -- commenting
  { from = '#',          to = 'v:lua.on_commentary(\'Commentary\')',                                            mode = { 'n', 'o', 'x' }, opt = { noremap = false, expr = true } },
  { from = '##',         to = 'v:lua.on_commentary(\'CommentaryLine\')',                                        mode = { 'n', 'o', 'x' }, opt = { noremap = false, expr = true } },

  -- Telescope
  { from = '<C-G>',      to = ':Telescope git_status<CR>',                                                      mode = { 'n'           },                                        },
  { from = '<C-P>',      to = ':Telescope find_files<CR>',                                                      mode = { 'n'           },                                        },
  { from = '<C-L>',      to = ':Telescope live_grep<CR>',                                                       mode = { 'n'           },                                        },
  { from = '<C-R>',      to = ':Telescope grep_string<CR>',                                                     mode = { 'n'           },                                        },
  { from = '<C-B>',      to = ':Telescope buffers<CR>',                                                         mode = { 'n'           },                                        },
  { from = '<C-H>',      to = ':Telescope oldfiles<CR>',                                                        mode = { 'n'           },                                        },

  -- highlighting
  { from = '<leader>sp', to = ':lua toggle_bool(\'wo\', \'spell\')<CR>',                                        mode = { 'n'           },                                        },
  { from = '<leader>se', to = ':lua toggle_bool(\'o\', \'hlsearch\')<CR>',                                      mode = { 'n'           },                                        },

  -- git
  { from = '[c',         to = '&diff ? "]c" : "<cmd>lua require(\'gitsigns\').prev_hunk()<CR>"',                mode = { 'n'           }, opt = { expr = true }                  },
  { from = ']c',         to = '&diff ? "]c" : "<cmd>lua require(\'gitsigns\').next_hunk()<CR>"',                mode = { 'n'           }, opt = { expr = true }                  },
  { from = 'gu',         to = ':lua require(\'gitsigns\').undo_stage_hunk()<CR>',                               mode = { 'n'           },                                        },
  { from = 'gs',         to = ':lua require(\'gitsigns\').stage_hunk()<CR>',                                    mode = { 'n'           },                                        },
  { from = 'gr',         to = ':lua require(\'gitsigns\').reset_hunk()<CR>',                                    mode = { 'n'           },                                        },
  { from = 'gb',         to = ':lua require(\'gitsigns\').blame_line()<CR>',                                    mode = { 'n'           },                                        },
  { from = 'gp',         to = ':lua require(\'gitsigns\').preview_hunk()<CR>',                                  mode = { 'n'           },                                        },

  -- Tree Sitter
  { from = '<C-S>',      to = ':lua require(\'nvim-treesitter.incremental_selection\').init_selection()<CR>',   mode = { 'n'           },                                        },
  { from = '<C-E>',      to = ':lua require(\'nvim-treesitter.incremental_selection\').node_incremental()<CR>', mode = { 'v',          },                                        },
  { from = '<C-N>',      to = ':lua require(\'nvim-treesitter.incremental_selection\').node_decremental()<CR>', mode = { 'v',          },                                        },

  -- LSP
  { from = 'fd',         to = ':lua vim.lsp.buf.definition()<CR>',                                              mode = { 'n'           },                                        },
  { from = 'tf',         to = ':lua _G.toggle_auto_format()<CR>',                                               mode = { 'n'           },                                        },
  { from = 'ff',         to = ':Format<CR>',                                                                    mode = { 'n'           },                                        },
  { from = 'E',          to = ':lua _G.on_hover()<CR>',                                                         mode = { 'n'           },                                        },
  { from = '[d',         to = ':lua vim.lsp.diagnostic.goto_prev()<CR>',                                        mode = { 'n'           },                                        },
  { from = ']d',         to = ':lua vim.lsp.diagnostic.goto_next()<CR>',                                        mode = { 'n'           },                                        },

  { from = '<CR>',       to = 'v:lua.on_insert_enter()'                                                       , mode = { 'i'           }, opt = { expr = true }                  },
  { from = '<TAB>',      to = 'v:lua.on_insert_tab()'                                                         , mode = { 'i'           }, opt = { expr = true }                  },
  { from = '<S-TAB>',    to = 'v:lua.on_insert_shift_tab()'                                                   , mode = { 'i'           }, opt = { expr = true }                  },
  { from = '<C-Space>',  to = 'compe#complete()'                                                              , mode = { 'i'           }, opt = { expr = true }                  },
};

for key, value in ipairs(keys) do
  for _, mode in ipairs(value.mode) do
    local default_opt = { noremap = true, silent = true };
    vim.api.nvim_set_keymap(mode, value.from, value.to, value.opt and require('utils-test').join_tables(default_opt, value.opt) or default_opt);
  end;
end;

local check_back_space = function()
    local col = vim.fn.col('.') - 1
    if col == 0 or vim.fn.getline('.'):sub(col, col):match('%s') then
        return true
    else
        return false
    end
end

_G.on_insert_tab = function()
  if vim.fn.pumvisible() ~= 0 then
    return autopairs.esc('<C-N>');
  elseif check_back_space() then
    return autopairs.esc('<TAB>');
  else
    return vim.fn['compe#complete']();
  end
end;

_G.on_insert_shift_tab = function()
  if vim.fn.pumvisible() ~= 0 then
    return autopairs.esc('<C-P>');
  else
    return autopairs.esc('<S-TAB>');
  end
end

_G.on_insert_enter = function()
  if vim.fn.pumvisible() ~= 0 then
    if vim.fn.complete_info()['selected'] == -1 then

      return autopairs.esc('<CR>');
    end

    return vim.fn["compe#confirm"](autopairs.esc('<c-r>'));
  end

  return autopairs.check_break_line_char();
end;

vim.g.auto_format_state = false;

_G.toggle_auto_format = function()
  if (vim.g.auto_format_state) then
    vim.api.nvim_exec([[
      augroup BWPAutoFormat
        autocmd!
      augroup END
    ]], false);
  else
    vim.api.nvim_exec([[
      augroup BWPAutoFormat
        autocmd!
        autocmd BufWritePost *.tsx,*.ts,*.rs FormatWrite
      augroup END
    ]], false);
  end

  vim.g.auto_format_state = not vim.g.auto_format_state;
end;

_G.on_hover = function()
  local filetype = vim.bo.filetype;

  if (filetype == 'vim' or filetype == 'help') then
    vim.cmd('h ' .. vim.fn.expand('<cword>'));
  else
    vim.lsp.buf.hover();
  end
end;
