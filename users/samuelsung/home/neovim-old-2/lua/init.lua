require('general')
require('filetypes');
require('layout');
require('navigation')
require('plugins.telescope');
require('plugins.autopairs');
require('plugins.compe');
require('plugins.treesitter');
require('plugins.lspconfig');
require('plugins.formatter');
require('plugins.gitsigns');

vim.g.ranger_map_keys = 0;
--vim.g.vifm_exec = 'vifmrun';

