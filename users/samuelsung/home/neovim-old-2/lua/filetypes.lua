vim.api.nvim_exec([[
  " Nix
  au BufRead,BufNewFile *.nix set filetype=nix

  " Java
  autocmd FileType java setlocal shiftwidth=4
  autocmd FileType java setlocal tabstop=4
]], false);
