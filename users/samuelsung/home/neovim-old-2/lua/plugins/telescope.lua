local tactions = require('telescope.actions');
require('telescope').setup({
  defaults = {
    vimgrep_arguments = {
      'rg',
      '--color=never',
      '--no-heading',
      '--with-filename',
      '--line-number',
      '--column',
      '--smart-case'
    },
    prompt_prefix = "> ",
    selection_caret = "> ",
    entry_prefix = "  ",
    initial_mode = "insert",
    selection_strategy = "reset",
    sorting_strategy = "descending",
    layout_strategy = "horizontal",
    layout_config = {
      horizontal = {
        mirror = false,
        width_padding = 0.05,
        height_padding = 0.05,
        preview_width = function(_, cols)
          if cols < 70 then
            return 0
          else
            return math.floor(cols * 0.65)
          end
        end,
      },
      vertical = {
        mirror = false,
      },
      prompt_position = "bottom",
      preview_cutoff = 70,
    },

    mappings = {
      i = {
        --['<ESC>'] = tactions.close,
        ['<C-P>'] = false,
        ['<C-N>'] = tactions.move_selection_next,
        ['<C-E>'] = tactions.move_selection_previous,
      },
      n = {
        ['<C-P>'] = false,
        ['<C-N>'] = tactions.move_selection_next,
        ['<C-E>'] = tactions.move_selection_previous,
      },
    },
  },
});
