local eslint = function()
  return {
    exe = 'eslint_d',
    args = {'--fix-to-stdout', '--stdin', '--stdin-filename', vim.api.nvim_buf_get_name(0), '--cache'},
    stdin = true,
  };
end;

local nixpkgs_fmt = function()
  return {
    exe = 'nixpkgs-fmt',
    stdin = true,
  };
end;

local rustfmt = function()
  return {
    exe = 'rustfmt',
    args = { '--emit=stdout' },
    stdin = true,
  };
end;

local shfmt = function()
  return {
    exe = 'shfmt',
    args = { '-ci', '-s', '-bn', '-i', '2' },
    stdin = true,
  };
end;

require('formatter').setup({
  logging = false,
  filetype = {
    javascript = {eslint},
    javascriptreact = {eslint},
    ['javascript.jsx'] = {eslint},
    typescript = {eslint},
    ['typescript.tsx'] = {eslint},
    typescriptreact = {eslint},
    rust = { rustfmt },
    nix = { nixpkgs_fmt },
    sh = {shfmt},
  },
})
