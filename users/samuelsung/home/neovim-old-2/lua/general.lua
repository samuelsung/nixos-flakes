-- Disable Swap File and Backup File
vim.bo.swapfile = false;
vim.o.swapfile = false;
vim.o.backup = false;
vim.o.writebackup = false;

-- Autoread
vim.o.autoread = true;
vim.bo.autoread = true;

-- Command History
vim.o.history = 500;

-- You will have bad experience for diagnostic messages when it's default 4000.
vim.o.updatetime = 200;

-- Use Unix as the standard file type
vim.o.fileformats = 'unix,dos,mac';

-- Set utf8 as standard encoding and en_US as the standard language
vim.o.encoding = 'utf8';

-- Hide Buffers instead of Closing Them
vim.o.hidden = true;

-- Ignore casing when searching
vim.o.ignorecase = true;
vim.o.incsearch = true;

-- Enable wildmenu for command completion
vim.o.wildmenu = true;
vim.o.wildmode = 'longest:full,full';

-- Show unfinished command
vim.o.showcmd = true;

-- Use System Clipboard
vim.o.clipboard = 'unnamedplus';

-- don't give |ins-completion-menu| messages.
vim.o.shortmess = vim.o.shortmess .. 'c';

-- Auto make directory if it does not exist
_G.mk_non_exist_dir = function(file, buf)
  if vim.fn.empty(vim.fn.getbufvar(buf, '&buftype')) == 1 then
    local dir = vim.fn.fnamemodify(file, ':h');
    if vim.fn.isdirectory(dir) == 0 then
      vim.fn.mkdir(dir, 'p');
    end
  end
end;

vim.api.nvim_exec([[
  augroup BWCCreateDir
    autocmd!
    autocmd BufWritePre * lua _G.mk_non_exist_dir(vim.fn.expand('<afile>'), vim.fn.expand('<abuf>'))
  augroup END
]], false);
