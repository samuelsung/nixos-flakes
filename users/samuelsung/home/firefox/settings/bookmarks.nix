{
  toolbar = [
    {
      isFolder = true;
      children = [

        {
          name = "work";
          isFolder = true;
          children = [
            { name = "ifs-gitlab"; keyword = "ifsgitlab"; url = "http://scm-server.ifshk.com/"; }
            { name = "ifs-homepage"; keyword = "ifshomepage"; url = "http://homepage.ifshk.com/"; }
            { name = "ifs-tsm"; keyword = "ifstsm"; url = "http://tsm.ifshk.com:8080/login/"; }
            { name = "ifs-odoo"; keyword = "ifsodoo"; url = "http://comweb.ifshk.com"; }
            { name = "ifs-webmail"; keyword = "ifswebmail"; url = "https://webmail.ifshk.com"; }
            { name = "Jira"; keyword = "jira"; url = "https://ifshk.atlassian.net/jira/your-work"; }
          ];
        }

        {
          name = "nix";
          isFolder = true;
          children = [

            {
              name = "repos";
              isFolder = true;
              children = [
                { name = "Mic92/sops-nix"; keyword = "sopsnix"; url = "https://github.com/Mic92/sops-nix"; }
                { name = "mozilla/sops"; keyword = "sops"; url = "https://github.com/mozilla/sops"; }
                { name = "nix-community/nur-combined"; keyword = "nurcombined"; url = "https://github.com/nix-community/nur-combined"; }
              ];
            }

            { name = "Nixpkgs Search"; keyword = "nixsearch"; url = "https://search.nixos.org"; }
          ];
        }

        {
          name = "references";
          isFolder = true;
          children = [

            {
              name = "React";
              isFolder = true;
              children = [
                { name = "react-hook-form"; keyword = "reacthookform"; url = "https://react-hook-form.com/"; }
              ];
            }
          ];
        }

        {
          name = "entertainment";
          isFolder = true;
          children = [

            {
              name = "games";
              isFolder = true;
              children = [

                {
                  name = "ファイナルファンタジーXIV";
                  isFolder = true;
                  children = [
                    { name = "The Lodestone"; keyword = "lodestone"; url = "https://jp.finalfantasyxiv.com/lodestone/"; }
                    { name = "FF14攻略 - Game8"; keyword = "game8ff14"; url = "https://game8.jp/ff14"; }
                  ];
                }
              ];
            }
          ];
        }

        { name = "gitlab"; keyword = "gitlab"; url = "https://gitlab.com/"; }
        { name = "selfhost gitlab"; keyword = "gitlabself"; url = "https://gitlab.samuelsung1998.net/"; }
      ];
    }
  ];
}
