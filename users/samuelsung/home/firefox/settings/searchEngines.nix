{ withGames ? false
, withSocialMedia ? false
, withShopping ? false
}:
{
  version = 6;
  engines = [
    {
      name = "Google";
      isAppProvided = true;
      hidden = true;
    }

    {
      name = "Bing";
      isAppProvided = true;
      hidden = true;
    }

    {
      name = "Amazon.com";
      isAppProvided = true;
      hidden = true;
    }

    {
      name = "DuckDuckGo";
      isAppProvided = true;
      alias = "";
    }

    {
      name = "NixOS packages";
      desc = "Search NixOS packages by name or description.";
      alias = "@nix,@nixpackage,@nixpkgs";
      loadPath = "[https]search.nixos.org/nixos-packages.xml";
      loadPathHash = "w0kzbgPE0DHe27P492iUFDqkNR86OUn0VnwGxF3YnlY=";
      searchForm = "https://search.nixos.org/packages";
      icon = ./icons/nix.png;
      urls = [
        {
          params = [ ];
          rels = [ ];
          template = "https://search.nixos.org/packages?query={searchTerms}";
        }
      ];
    }

    {
      name = "NixOS options";
      desc = "Search NixOS options by name or description.";
      alias = "@nixoptions,@nixopt";
      loadPath = "[https]search.nixos.org/nixos-options.xml";
      loadPathHash = "UzvXf343EDf3XTpEo3Q8nIX4kV8xdospLcEPqhGrpsI=";
      searchForm = "https://search.nixos.org/options";
      icon = ./icons/nix.png;
      urls = [
        {
          params = [ ];
          rels = [ ];
          template = "https://search.nixos.org/options?query={searchTerms}";
        }
      ];
    }

    {
      name = "NixOS Wiki (en)";
      desc = "NixOS Wiki (en)";
      alias = "@nixwiki,@nixoswiki";
      loadPath = "[https]nixos.wiki/nixos-wiki-en.xml";
      loadPathHash = "Jk8ydMRlQqc/J1ucplBjpVlgGEFBopExtdQwPAedwJw=";
      searchForm = "https://nixos.wiki/wiki/Special:Search";
      icon = ./icons/nix.png;
      urls = [
        {
          params = [ ];
          rels = [ ];
          template = "https://nixos.wiki/index.php?title=Special:Search&search={searchTerms}";
        }
        {
          params = [ ];
          rels = [ ];
          template = "https://nixos.wiki/api.php?action=opensearch&search={searchTerms}&namespace=0";
          type = "application/x-suggestions+json";
        }
        {
          params = [ ];
          rels = [ ];
          template = "https://nixos.wiki/api.php?action=opensearch&format=xml&search={searchTerms}&namespace=0";
          type = "application/x-suggestions+xml";
        }
      ];
    }

    {
      name = "ArchWiki (en)";
      desc = "ArchWiki (en)";
      alias = "@arch,@archwiki";
      loadPath = "[https]wiki.archlinux.org/archwiki-en.xml";
      loadPathHash = "k/YS9abSP4yaxDIdlPStQfELMc61ISzuwM/m4X/lZ1M=";
      searchForm = "https://wiki.archlinux.org/title/Special:Search";
      icon = ./icons/arch.ico;
      iconType = "vnd.microsoft.icon";
      urls = [
        {
          params = [ ];
          rels = [ ];
          template = "https://wiki.archlinux.org/index.php?title=Special:Search&search={searchTerms}";
        }
        {
          params = [ ];
          rels = [ ];
          template = "https://wiki.archlinux.org/api.php?action=opensearch&search={searchTerms}&namespace=0|3000";
          type = "application/x-suggestions+json";
        }
        {
          params = [ ];
          rels = [ ];
          template = "https://wiki.archlinux.org/api.php?action=opensearch&format=xml&search={searchTerms}&namespace=0|3000";
          type = "application/x-suggestions+xml";
        }
      ];
    }

    {
      name = "GitLab";
      desc = "Search GitLab";
      alias = "@gitlab";
      loadPath = "[https]gitlab.com/gitlab.xml";
      loadPathHash = "fxlI/5/fh7ZaZ02ovSdzIQAU6/WVBOY1lhhrShH0nxE=";
      searchForm = "https://gitlab.com/search";
      icon = ./icons/gitlab.png;
      urls = [
        {
          params = [ ];
          rels = [ ];
          template = "https://gitlab.com/search?search={searchTerms}";
        }
      ];
    }

    {
      name = "GitHub";
      desc = "Search GitHub";
      alias = "@github";
      loadPath = "[https]github.com/github.xml";
      loadPathHash = "Um+tA74PLFGR0+OKcUN8wY6+X/q+Hvhp8hcWTZfbIPA=";
      searchForm = "https://github.com/search";
      icon = ./icons/github.ico;
      iconType = "vnd.microsoft.icon";
      urls = [
        {
          params = [ ];
          rels = [ ];
          template = "https://github.com/search?q={searchTerms}&ref=opensearch";
        }
      ];
    }

    {
      name = "Yarn";
      desc = "Yarn package search";
      alias = "@yarn";
      loadPath = "[https]yarnpkg.com/yarn.xml";
      loadPathHash = "HzJfZA2oM/syqtownE+yvi7/N8JT8O7Z3abvjRh1asU=";
      searchForm = "https://yarnpkg.com";
      icon = ./icons/yarn.png;
      urls = [
        {
          params = [ ];
          rels = [ ];
          template = "https://yarnpkg.com/?q={searchTerms}";
        }
      ];
    }

    {
      name = "npm";
      desc = "Search for packages on npm";
      alias = "@npm";
      loadPath = "[https]static.npmjs.com/npm.xml";
      loadPathHash = "nohePI3XrKnsWQw6K7ssMKKwHWA3+jNtYKUNjNxJoiI=";
      searchForm = "https://www.npmjs.com";
      icon = ./icons/npm.png;
      urls = [
        {
          params = [ ];
          rels = [ ];
          template = "https://www.npmjs.com/search?q={searchTerms}";
        }
      ];
    }

    {
      name = "Cargo";
      desc = "Search for crates on crates.io";
      alias = "@crates,@cargo";
      loadPath = "[https]crates.io/cargo.xml";
      loadPathHash = "+PFAehmtpdAiBnL/2QngW7qzinsBNISD8quGXiNW3j8=";
      icon = ./icons/cargo.png;
      urls = [
        {
          params = [ ];
          rels = [ ];
          template = "https://crates.io/search?q={searchTerms}";
        }
      ];
    }

    {
      name = "Docs.rs";
      desc = "Search for crate documentation on docs.rs";
      alias = "docsrs";
      loadPath = "[https]docs.rs/docsrs.xml";
      loadPathHash = "BXUZ9dRM/aOarJIfGteleqZG8py0Dlf/XQGyMgvSisI=";
      icon = ./icons/docsrs.png;
      urls = [
        {
          params = [ ];
          rels = [ ];
          template = "https://docs.rs/releases/search?query={searchTerms}";
        }
      ];
    }

    {
      name = "Hoggle";
      desc = "Hoogle is a Haskell API search engine, which allows you to search many standard Haskell libraries by either function name, or by approximate type signature.";
      alias = "@hoggle";
      loadPath = "[https]hoogle.haskell.org/hoogle.xml";
      loadPathHash = "0K6qMpO7q/8Z9T7b+x38T2gtk+PunTQaOGkdZp8l0xE=";
      icon = ./icons/hoggle.png;
      urls = [
        {
          params = [ ];
          rels = [ ];
          template = "https://hoogle.haskell.org/?hoogle={searchTerms}";
        }
        {
          params = [ ];
          rels = [ ];
          template = "https://hoogle.haskell.org/?hoogle={searchTerms}&mode=suggest";
          type = "application/x-suggestions+json";
        }
      ];
    }

    {
      name = "Wikipedia (en)";
      isAppProvided = true;
      alias = "@wiki,@wikien";
    }

    {
      name = "Wikipedia (ja)";
      desc = "Wikipedia (ja)";
      alias = "@wikijp";
      loadPath = "[https]ja.wikipedia.org/wikipedia-ja.xml";
      loadPathHash = "jW5b7mp+KuSgKbn2W5a444FgNh/XTf0xZAbIJKjkNsM=";
      searchForm = "https://ja.wikipedia.org/wiki/%E7%89%B9%E5%88%A5:%E6%A4%9C%E7%B4%A2";
      icon = ./icons/wiki.png;
      urls = [
        {
          params = [ ];
          rels = [ ];
          template = "https://ja.wikipedia.org/w/index.php?title=%E7%89%B9%E5%88%A5:%E6%A4%9C%E7%B4%A2&search={searchTerms}";
        }
      ];
    }

  ] ++ (if withGames then [

    {
      name = "Final Fantasy XIV The Lodestone";
      alias = "@lodestone";
      loadPath = "[other]addEngineWithDetails:set-via-user";
      searchForm = "https://jp.finalfantasyxiv.com";
      icon = ./icons/lodestone.ico;
      iconType = "vnd.microsoft.icon";
      urls = [
        {
          params = [ ];
          rels = [ ];
          template = "https://jp.finalfantasyxiv.com/lodestone/community/search/?q={searchTerms}";
        }
      ];
    }

    {
      name = "FF14攻略 - Game8";
      desc = "FF14攻略 - Game8";
      alias = "@game8ff14";
      loadPath = "[other]addEngineWithDetails:set-via-user";
      searchForm = "https://game8.jp";
      icon = ./icons/game8.png;
      urls = [
        {
          params = [ ];
          rels = [ ];
          template = "https://game8.jp/ff14/search?q={searchTerms}";
        }
      ];
    }

  ] else [ ]) ++ (if withSocialMedia then [

    {
      name = "YouTube";
      desc = "Search for videos on YouTube";
      alias = "@youtube";
      loadPath = "[https]www.youtube.com/youtube.xml";
      loadPathHash = "n+QnnEVlmB5QYl4LHYCxCmRr3K+I05owfjTI9RHtGYk=";
      searchForm = "https://www.youtube.com";
      icon = ./icons/youtube.png;
      urls = [
        {
          params = [ ];
          rels = [ ];
          template = "https://www.youtube.com/results?search_query={searchTerms}&page={startPage?}&utm_source=opensearch";
        }
      ];
    }

    {
      name = "Twitter";
      desc = "Twitter Search";
      alias = "@twitter";
      loadPath = "[https]twitter.com/twitter.xml";
      loadPathHash = "3Nm9eG7WyWr5JZD8hONUzCq5O81R4IgHU7phDvrXbiw=";
      searchForm = "https://twitter.com/search-home";
      icon = ./icons/twitter.ico;
      iconType = "vnd.microsoft.icon";
      urls = [
        {
          params = [ ];
          rels = [ ];
          template = "https://twitter.com/search?q={searchTerms}";
        }
      ];
    }

    {
      name = "Reddit";
      alias = "@reddit";
      loadPath = "[other]addEngineWithDetails:set-via-user";
      searchForm = "https://www.reddit.com";
      icon = ./icons/reddit.png;
      urls = [
        {
          params = [ ];
          rels = [ ];
          template = "https://www.reddit.com/search/?q={searchTerms}";
        }
      ];
    }

    {
      name = "LIHKG";
      alias = "@lihkg";
      loadPath = "[other]addEngineWithDetails:set-via-user";
      searchForm = "https://lihkg.com";
      icon = ./icons/lihkg.ico;
      iconType = "vnd.microsoft.icon";
      urls = [
        {
          params = [ ];
          rels = [ ];
          template = "https://lihkg.com/search?q={searchTerms}";
        }
      ];
    }

  ] else [ ]) ++ (if withShopping then [

    {
      name = "Amazon Search Suggestions";
      alias = "@amazon,@amazonjp";
      loadPath = "[https]d2lo25i6d3q8zm.cloudfront.net/amazon-search-suggestions.xml";
      loadPathHash = "BaD0Ly8q4Pe+LZpqZSKu/m2AoSRyCf35TZkye16Oe3k=";
      icon = ./icons/amazon.ico;
      iconType = "vnd.microsoft.icon";
      urls = [
        {
          params = [ ];
          rels = [ ];
          template = "http://www.amazon.com/s/ref=azs_osd_firefox?tag=amznsearchff-20&link%5Fcode=qs&index=aps&field-keywords={searchTerms}";
        }
        {
          params = [ ];
          rels = [ ];
          template = "http://completion.amazon.com/search/complete?method=completion&q={searchTerms}&search-alias=aps&client=amzn-search-suggestions/9fe582406fb5106f343a84083d78795713c12d68&mkt=1";
          type = "application/x-suggestions+json";
        }
        {
          params = [ ];
          rels = [ ];
          template = "http://d2lo25i6d3q8zm.cloudfront.net/browser-plugins/AmazonSearchSuggestionsOSD.Firefox.xml";
          type = "application/opensearchdescription+xml";
        }
      ];
    }
  ] else [ ]);

  metaData = {
    useSavedOrder = true;
    current = "DuckDuckGo";
    hash = "SAwnRqalCPASel0MooiVGYhRsMN6LGAxjSZ0SFJCZIY=";
  };
}
