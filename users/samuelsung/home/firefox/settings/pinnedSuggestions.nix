[
  {
    url = "https://gitlab.com/";
    label = "gitlab";
    baseDomain = "gitlab.com";
  }
  {
    url = "https://github.com/mozilla/sops";
    label = "github";
    baseDomain = "github.com";
  }
  {
    url = "https://account.protonmail.com/login";
    label = "pontonmail";
    customScreenshotURL = "https://protonmail.com/apple-touch-icon.png";
  }
  {
    url = "https://search.nixos.org/packages";
    label = "nixpkgs";
  }
  {
    url = "https://e-banking.hangseng.com/";
    label = "hangseng";
    customScreenshotURL = "https://www.hangseng.com/etc/designs/hase/favicon.ico";
  }
  {
    url = "https://www.aeon.com.hk/netmember/message.do?method=displayConcurrentLogin";
    label = "aeon";
  }
  {
    url = "https://www.ppshk.com/pps/pps2/revamp2/template/pc/login_c.jsp";
    label = "pps";
  }
  {
    url = "https://wiki.archlinux.org/";
    label = "archwiki";
    customScreenshotURL = "https://archlinux.org/static/logos/apple-touch-icon-144x144.38cf584757c3.png";
  }
]
