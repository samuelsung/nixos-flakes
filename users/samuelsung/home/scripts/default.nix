{ config, lib, pkgs, ... }:

with lib;
let
  cfg = config.systemCustomize;
in
{
  home.packages = with pkgs; mkIf (cfg.type == "desktop") [
    (writeShellScriptBin "samedir" ./samedir)
    (writeShellScriptBin "dmenu_emoji" ./dmenu_emoji)
    (writeShellScriptBin "youtube-dl-mpv" ./youtube-dl-mpv)
  ];
}
