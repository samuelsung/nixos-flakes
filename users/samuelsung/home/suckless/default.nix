{ config, lib, pkgs, ... }:

with lib;
let
  cfg = config.systemCustomize;
in
mkIf (cfg.type == "desktop") {
  home.packages = with pkgs; [ dmenu dwm st ];
}
