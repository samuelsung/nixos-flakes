{ config, lib, pkgs, ... }:

with lib;
let
  cfg = config.systemCustomize;
in
mkIf (cfg.type == "desktop") {
  home.packages = with pkgs; [
    dbus
  ];

  xsession.windowManager.xmonad = {
    enable = true;
    enableContribAndExtras = true;
    extraPackages = haskellPackages: [
      haskellPackages.dbus
      pkgs.dbus
    ];
    config = ./xmonad.hs;
  };

  home.file.".xmonad/polybar_xmonad.sh".source = ./polybar_xmonad.sh;
}
