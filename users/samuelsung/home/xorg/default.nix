{ config, lib, pkgs, ... }:

with lib;
let
  cfg = config.systemCustomize;

  wallpapers = (import ./wallpapers pkgs);

  loopwallpapers = pkgs.writeShellScriptBin "loopwallpapers" ''
    pgrep loopwallpapers | grep -v ^$$\$ | xargs kill

    while true; do {
      feh --randomize --bg-fill ${wallpapers};
      sleep 600;
    }; done
  '';
in
mkIf (cfg.type == "desktop") {
  home.packages = with pkgs; [
    feh
    networkmanagerapplet
    loopwallpapers
  ];

  home.file.".Xresources".source = ./.Xresources;
  home.file.".xinitrc".source = ./.xinitrc;
}
