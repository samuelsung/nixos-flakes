{ pkgs, ... }:

let
  inherit (builtins) isAttrs concatStringsSep map hasAttr;
  inherit (pkgs) fetchurl runCommand;
  inherit (pkgs.lib) flatten mapAttrsRecursive mapAttrsToList;

  flattenValue = list: flatten (mapAttrsToList
    (_: v: (
      if (isAttrs v && !(hasAttr "url" v)) then
        flattenValue v
      else
        v
    ))
    list
  );

  mapLinkNodes = mapAttrsRecursive (path: value: {
    url = concatStringsSep "/" path;
    sha256 = value;
  });

  mapLinks = attrs: flattenValue (mapLinkNodes attrs);

  mapLinkSources = map ({ url, sha256 }: fetchurl { inherit url sha256; });

  mapSourcesToFolder = sources: (runCommand "mapSourcesToFolder" { }
    ''
      mkdir "$out";
      ${concatStringsSep "\n" (
        map
          (s: ''cp "${s}" "$out"'')
          sources
      )}
    '');

  mapLinksToFolder = links: mapSourcesToFolder (mapLinkSources (mapLinks links));
in
mapLinksToFolder {
  "https://gitlab.com/samuelsung/ff14ss/-/raw/master/ss/2021-11-30" = {
    "2021-11-30_18-48-27-178_Talim_-_Thank_You.png" = "sha256-OS19hBQynwoVZ8GSdr+R6us2T4/+IdV1y0faXOBmZrE=";
    "2021-11-30_19-01-26-735_Talim_-_Thank_You.png" = "sha256-i4mcwlmjamxpCtPSzc0qqk9MAWxKGU6ygZT+WX2VaiU=";
    "2021-11-30_18-56-13-171_Talim_-_Thank_You.png" = "sha256-gHtZ8pLRbTdOHUUq2SRzaWeQPaeghtUPLZ+35m4HVVI=";
    "2021-11-30_19-21-34-767_Talim_-_Bloodmoon.png" = "sha256-vFg40c3DF6V34a63KvxWjnZKNgpia73NYzfEvYerAG4=";
    "2021-11-30_19-25-13-332_Talim_-_Bloodmoon.png" = "sha256-Q2w1/uUTD0pcvLlDYSzWBaMKwt7LUBj2JTrEiTZTmM4=";
    "2021-11-30_19-28-15-146_Talim_-_Bloodmoon.png" = "sha256-ip29rsxYLruZYoSj9QEng9NCKzYr5RrQ821Fxh1SF3M=";
  };
}
