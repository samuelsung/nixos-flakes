{ config, lib, pkgs, ... }:

with lib;

let
  cfg = config.systemCustomize;
in
mkIf (cfg.type == "desktop") {
  home.packages = with pkgs; [
    libnotify
    dunst
  ];

  home.file.".config/dunst/dunstrc".source = ./dunstrc;
}
