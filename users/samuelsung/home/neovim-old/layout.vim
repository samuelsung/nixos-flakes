" Splitting
set splitbelow
set splitright

" Set the command prompt to the bottom line
set cmdheight=1

" Left 10 lines at the end
set so=10

" always show signcolumns
set signcolumn=yes

" Folding
set foldmethod=syntax
"set foldcolumn=1
set foldlevelstart=99

" List Characters
set list listchars=eol:¬,tab:▸\ ,trail:.,

" Enable Counter
set ruler

" Enable the number column
set number relativenumber

" Enable syntax highlighting
syntax on
syntax enable

" always show signcolumns
set signcolumn=yes

" Enable spell checking highlight
" let g:gruvbox_guisp_fallback='bg'

" Set Gruvbox High Contrast Mode
" let g:gruvbox_contrast_dark = 'soft'
" let g:gruvbox_material_background = 'hard'

" Enable 256 colors palette in Gnome Terminal
if $COLORTERM == 'gnome-terminal'
  set t_Co=256
endif

" Enable Dark Mode
set background=dark

" Use Truecolorscheme
set termguicolors

let g:nord_cursor_line_number_background=1

" Enable Color Scheme
" colorscheme gruvbox
colorscheme nord

" indentLine colors
" Nord
" let g:indentLine_color_gui = '#4c566a'
" let g:indentLine_bgcolor_gui = '#FF5F00'

" Disable Background for Transparency
" autocmd VimEnter * 
" highlight NonText ctermbg=NONE guibg=NONE
highlight Normal ctermbg=NONE guibg=NONE
highlight LineNr ctermbg=NONE guibg=NONE
highlight SignColumn ctermbg=NONE guibg=NONE

" Airline
" Use Powerline Fonts
let g:airline_powerline_fonts = 1

" Theme
let g:airline_theme = 'nord'

" Extensions
" Tabline
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#formatter = 'default'
let g:airline#extensions#tabline#buffer_nr_show = 1

" Add status line support, for integration with other plugin, checkout `:h coc-status`
set statusline^=%{coc#status()}%{get(b:,'coc_current_function','')}

