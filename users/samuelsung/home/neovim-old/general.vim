" Disable Swap File and Backup File
set noswapfile
set nobackup
set nowritebackup

" Autoread
set autoread

" Autoread Watcher
function! Autoread(timer)
  silent checktime %
endfunction!
let timer = timer_start(1000, 'Autoread', {'repeat': -1})

" disable ex mode
nnoremap Q <nop>

" Set NeoVim to remember 500 lines
set history=500

" You will have bad experience for diagnostic messages when it's default 4000.
set updatetime=100

" Use Unix as the standard file type
set ffs=unix,dos,mac

" Set utf8 as standard encoding and en_US as the standard language
set encoding=utf8

" Hide Buffers instead of Closing Them
set hidden

" Ignore casing when searching
set ignorecase
set incsearch

" Enable wildmenu for command completion
set wildmenu
set wildmode=longest:full,full

" Show unfinished command
set showcmd

" don't give |ins-completion-menu| messages.
set shortmess+=c

" Auto make directory if it does not exist
function s:MkNonExDir(file, buf)
  if empty(getbufvar(a:buf, '&buftype')) && a:file!~#'\v^\w+\:\/'
    let dir=fnamemodify(a:file, ':h')
    if !isdirectory(dir)
      call mkdir(dir, 'p')
    endif
  endif
endfunction
augroup BWCCreateDir
  autocmd!
  autocmd BufWritePre * :call s:MkNonExDir(expand('<afile>'), +expand('<abuf>'))
augroup END

" Use system clipboard
set clipboard+=unnamedplus
