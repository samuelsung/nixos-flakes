" NERDCommenter Add spaces after comment delimiters by default
let g:NERDSpaceDelims = 1

" NERDCommenter Use compact syntax for prettified multi-line comments
let g:NERDCompactSexyComs = 1

" NERDCommenter Align line-wise comment delimiters flush left instead of following code indentation
let g:NERDDefaultAlign = 'left'

