let g:blamer_enabled = 1
let g:blamer_delay = 1000
nnoremap <leader>bl :BlamerToggle<CR>
