" Auto indent, Smart indent, and auto wrap line
set autoindent
set smartindent
set wrap
set cindent

" Use Spaces instead of Tabs
set expandtab

" Enable Smarttab
set smarttab

" Set the tab to 4 spaces
set shiftwidth=2
set tabstop=2

