" JSON
" Disable JSON conceal (Qoutes would not be hidden)
if !exists('g:vscode')
  let g:vim_json_syntax_conceal = 0
endif

" Javascript
if !exists('g:vscode')
  let javaScript_fold=1
  let g:vim_jsx_pretty_colorful_config=1
endif

" Rust
if !exists('g:vscode')
  let g:rustfmt_autosave = 1
  autocmd FileType rust setlocal shiftwidth=2
  autocmd FileType rust setlocal tabstop=2
endif

"Markdown
if !exists('g:vscode')
  let g:vim_markdown_conceal = 0
  let g:vim_markdown_math = 1
  let g:tex_conceal = ""
  let g:vim_markdown_conceal_code_blocks = 0
  let g:ycm_show_diagnostics_ui = 0
  autocmd FileType markdown setlocal noautoindent
  autocmd FileType markdown setlocal nosmartindent
endif

" PHP
if !exists('g:vscode')
  autocmd FileType php setlocal autoindent
endif

" Java
if !exists('g:vscode')
  autocmd FileType java setlocal shiftwidth=4
  autocmd FileType java setlocal tabstop=4
endif

