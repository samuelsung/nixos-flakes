{ lib, config, pkgs, ... }:

with lib;
let
  cfg = config.systemCustomize;
in
mkIf (cfg.type == "desktop") {
  services.flameshot.enable = true;
  # Whether to enable Flameshot.
  # Type: boolean
  # Default: false
  # Example: true
  # Declared by:
  # <home-manager/modules/services/flameshot.nix> 

  home.file.".config/flameshot/flameshot.config".source = ./flameshot.ini;
}
