{ lib, config, ... }:

with lib;
let
  cfg = config.systemCustomize;
in
{
  imports = [
    ./common/system
    (import ./samuelsung/system { username = "samuelsung"; })
    (import ./samuelsung_play/system { username = "samuelsung_play"; })
  ];

  home-manager.sharedModules = [ ./common/home ];

  home-manager.users = {
    samuelsung = mkIf (elem "samuelsung" cfg.users) {
      imports = [
        ./samuelsung/home
        (import ./shared/info { username = "samuelsung"; })
      ];
    };
    samuelsung_play = mkIf (elem "samuelsung_play" cfg.users) {
      imports = [
        ./samuelsung_play/home
        (import ./shared/info { username = "samuelsung_play"; })
      ];
    };
  };
}
