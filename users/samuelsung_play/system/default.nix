{ username }:

{ lib, config, pkgs, ... }@args:

with lib;
let
  cfg = config.systemCustomize;
in
mkMerge (map
  (p: mkIf (elem username cfg.users) (import p { inherit username; } args))
  [
    ../../samuelsung/system/lab
    ../../samuelsung/system/mpd
    ../../samuelsung/system/nix-direnv
    ../../samuelsung/system/ssh
    ./firefox
    ./secret-scripts
  ]
)
