{ username }:

{ lib, config, ... }:

with lib;
let
  cfg = config.systemCustomize;
in
mkIf ((cfg.type == "desktop") && (elem "games" cfg.services)) {
  sops.secrets."helper" = {
    format = "binary";
    sopsFile = ./helper.secret.sh;
    mode = "0550";
    owner = username;
    path = "/home/${username}/.local/bin/helper";
  };
}
