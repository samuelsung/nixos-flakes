{ username }:

{ lib, config, ... }:

with lib;
let
  cfg = config.systemCustomize;
in
mkIf (cfg.type == "desktop") {
  sops.secrets."bookmark.html" = {
    format = "binary";
    sopsFile = ./bookmarks.secret;
    mode = "0400";
    owner = username;
  };
}
