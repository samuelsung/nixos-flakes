{ config, lib, pkgs, ... }:

{
  home.packages = with pkgs; [
    # Remote
    moonlight-qt
  ];
}
