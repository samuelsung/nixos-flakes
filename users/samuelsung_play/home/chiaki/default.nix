{ config, lib, pkgs, ... }:

with lib;
let
  cfg = config.systemCustomize;
in
mkIf ((cfg.type == "desktop") && (elem "games" cfg.services)) {
  home.packages = with pkgs; [
    chiaki
  ];
}
