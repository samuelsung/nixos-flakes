{ lib, config, pkgs, ... }:

with lib;

let
  cfg = config.systemCustomize;
in
mkIf ((cfg.type == "desktop") && (elem "games" cfg.services)) {
  home.packages = with pkgs; [
    # Mini games
    kmines
    (custom.osu.override {
      appimageTools = pkgs.appimageTools.override {
        buildFHSUserEnv = pkgs.buildFHSUserEnv;
      };
    })
    lutris
    steam
    unstable.protontricks
  ];
}
