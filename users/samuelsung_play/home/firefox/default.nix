{ pkgs, lib, config, ... }:

with lib;
with (import ../../../shared/firefox/lib { inherit pkgs lib; });

let
  cfg = config.systemCustomize;
  settings = pkgs.callPackage ../../../samuelsung/home/firefox/settings { withProxyBlock = false; };
  searchEngines = mkSearchFile "samuelsung_play" (import ../../../samuelsung/home/firefox/settings/searchEngines.nix {
    withGames = true;
    withSocialMedia = true;
    withShopping = true;
  });
in
mkIf (cfg.type == "desktop") {
  programs.firefox.enable = true;
  # Whether to enable Firefox.
  # Type: boolean
  # Default: false
  # Example: true
  # Declared by:
  # <home-manager/modules/programs/firefox.nix> 

  # programs.firefox.enableGnomeExtensions = false;
  # Whether to enable the GNOME Shell native host connector. Note, you also need to set the NixOS option services.gnome3.chrome-gnome-shell.enable to true.
  # Type: boolean
  # Default: false
  # Declared by:
  # <home-manager/modules/programs/firefox.nix> 

  programs.firefox.package =
    pkgs.firefox.override {
      extraPrefs = mapLockPref settings.permanentSettings;
    };
  # The Firefox package to use. If state version ≥ 19.09 then this should be a wrapped Firefox package. For earlier state versions it should be an unwrapped Firefox package.
  # Type: package
  # Default: pkgs.firefox
  # Declared by:
  # <home-manager/modules/programs/firefox.nix> 

  programs.firefox.extensions = with pkgs.nur.repos.rycee.firefox-addons; [
    https-everywhere
    privacy-badger
    vimium
    h264ify
    bitwarden
    reduxdevtools
    react-devtools
    ublock-origin
  ];
  # List of Firefox add-on packages to install. Some pre-packaged add-ons are accessible from NUR, https://github.com/nix-community/NUR. Once you have NUR installed run
  #   $ nix-env -f '<nixpkgs>' -qaP -A nur.repos.rycee.firefox-addons
  # to list the available Firefox add-ons.
  # Note that it is necessary to manually enable these extensions inside Firefox after the first installation.
  # Extensions listed here will only be available in Firefox profiles managed through the programs.firefox.profiles option. This is due to recent changes in the way Firefox handles extension side-loading.
  # Type: list of packages
  # Default: [ ]
  # Example:
  # with pkgs.nur.repos.rycee.firefox-addons; [
  #   https-everywhere
  #   privacy-badger
  # ]
  # Declared by:
  # <home-manager/modules/programs/firefox.nix> 

  programs.firefox.profiles = {
    default = {
      id = 0;
      isDefault = true;
      settings = settings.oneTimeSettings;
    };

    secret = {
      id = 1;
      isDefault = false;

      settings = settings.oneTimeSettings // {
        # NOTE: bookmark.html will be updated on nix-rebuild since sops-nix does not support home manager yet.
        # see: https://github.com/Mic92/sops-nix/issues/62
        "browser.bookmarks.file" = "/run/secrets/bookmark.html";
      };
    };
  };

  # Attribute set of Firefox profiles.
  # Type: attribute set of submodules
  # Default: { }
  # Declared by:
  # <home-manager/modules/programs/firefox.nix> 

  # programs.firefox.profiles.<name>.bookmarks
  # Preloaded bookmarks. Note, this may silently overwrite any previously existing bookmarks!
  # Type: attribute set of submodules
  # Default: { }
  # Example:
  # {
  #   wikipedia = {
  #     keyword = "wiki";
  #     url = "https://en.wikipedia.org/wiki/Special:Search?search=%s&go=Go";
  #   };
  #   "kernel.org" = {
  #     url = "https://www.kernel.org";
  #   };
  # }
  # Declared by:
  # <home-manager/modules/programs/firefox.nix> 

  # programs.firefox.profiles.<name>.bookmarks.<name>.keyword
  # Bookmark search keyword.
  # Type: null or string
  # Default: null
  # Declared by:
  # <home-manager/modules/programs/firefox.nix> 

  # programs.firefox.profiles.<name>.bookmarks.<name>.name
  # Bookmark name.
  # Type: string
  # Default: "‹name›"
  # Declared by:
  # <home-manager/modules/programs/firefox.nix> 
  # programs.firefox.profiles.<name>.bookmarks.<name>.url
  # Bookmark url, use %s for search terms.
  # Type: string
  # Declared by:
  # <home-manager/modules/programs/firefox.nix> 

  # programs.firefox.profiles.<name>.extraConfig
  # Extra preferences to add to user.js.
  # Type: strings concatenated with "\n"
  # Default: ""
  # Declared by:
  # <home-manager/modules/programs/firefox.nix> 

  # programs.firefox.profiles.<name>.id
  # Profile ID. This should be set to a unique number per profile.
  # Type: unsigned integer, meaning >=0
  # Default: 0
  # Declared by:
  # <home-manager/modules/programs/firefox.nix> 

  # programs.firefox.profiles.<name>.isDefault
  # Whether this is a default profile.
  # Type: boolean
  # Default: "true if profile ID is 0"
  # Declared by:
  # <home-manager/modules/programs/firefox.nix> 

  # programs.firefox.profiles.<name>.name
  # Profile name.
  # Type: string
  # Default: "‹name›"
  # Declared by:
  # <home-manager/modules/programs/firefox.nix> 

  # programs.firefox.profiles.<name>.path
  # Profile path.
  # Type: string
  # Default: "‹name›"
  # Declared by:
  # <home-manager/modules/programs/firefox.nix> 

  # programs.firefox.profiles.<name>.settings
  # Attribute set of Firefox preferences.
  # Type: attribute set of boolean or signed integer or strings
  # Default: { }
  # Example:
  # {
  #   "browser.startup.homepage" = "https://nixos.org";
  #   "browser.search.region" = "GB";
  #   "browser.search.isUS" = false;
  #   "distribution.searchplugins.defaultLocale" = "en-GB";
  #   "general.useragent.locale" = "en-GB";
  #   "browser.bookmarks.showMobileBookmarks" = true;
  # }
  # Declared by:
  # <home-manager/modules/programs/firefox.nix> 

  # programs.firefox.profiles.<name>.userChrome
  # Custom Firefox user chrome CSS.
  # Type: strings concatenated with "\n"
  # Default: ""
  # Example:
  # ''
  # /* Hide tab bar in FF Quantum */
  # @-moz-document url("chrome://browser/content/browser.xul") {
  #   #TabsToolbar {
  #     visibility: collapse !important;
  #     margin-bottom: 21px !important;
  #   }

  #   #sidebar-box[sidebarcommand="treestyletab_piro_sakura_ne_jp-sidebar-action"] #sidebar-header {
  #     visibility: collapse !important;
  #   }
  # }
  # ''
  # Declared by:
  # <home-manager/modules/programs/firefox.nix> 

  # programs.firefox.profiles.<name>.userContent
  # Custom Firefox user content CSS.
  # Type: strings concatenated with "\n"
  # Default: ""
  # Example:
  # ''
  # /* Hide scrollbar in FF Quantum */
  # *{scrollbar-width:none !important}
  # ''
  # Declared by:
  # <home-manager/modules/programs/firefox.nix> 

  # Set default search to DuckDuckGo.
  # Also set search shortcuts
  home.file = {
    ".mozilla/firefox/default/search.json.mozlz4" = {
      source = searchEngines;
      force = true;
    };

    ".mozilla/firefox/secret/search.json.mozlz4" = {
      source = searchEngines;
      force = true;
    };
  };
}
