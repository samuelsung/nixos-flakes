{ config, lib, pkgs, ... }:

with lib;
let
  cfg = config.systemCustomize;

  samuelsung_common = {
    isNormalUser = true;
    initialPassword = "p@ssw0rd";
    extraGroups = mkMerge [
      [
        "wheel" # Enable ‘sudo’ for the user.
        "networkmanager" # Enable ‘networkmanager’ for the user.
        "samuelsung_common"
      ]

      (mkIf (config.virtualisation.libvirtd.enable) [
        # Enable kvm control
        "libvirtd"
      ])

      (mkIf (config.virtualisation.docker.enable) [
        # Enable docker control
        "docker"
      ])

      (mkIf (config.programs.light.enable) [
        # Enable light control
        "video"
      ])
    ];

    # TODO(samuelsung): Remove this after finish all migration
    openssh.authorizedKeys.keys = import ../../../samuelsung/system/ssh/authorizedKeys.nix;
  };
in
{
  users = {
    users.samuelsung_play = mkIf (elem "samuelsung_play" cfg.users) (samuelsung_common // {
      # To change uid, run
      # usermod -u <new_uid> foo # assign new uid to the user
      # find / -user <old_uid> -exec chown -h foo {} \; # apply the new uid to files

      # To change username
      # FIRST, uninstall home manager config
      # nix-env -e home-manager-path

      # run
      # usermod -l <new_name> <old_name>

      # move home folder
      # mv /home/<old_name> /home/<new_name>

      # fixed failed symlink which references the absolute path
      # new_name=<new_name>
      # old_name=<old_name>
      # find . -type l | while read -r p; do {
      #     if readlink "$p" | grep "/home/${old_name}/" > /dev/null; then {
      #         from=$p
      #         echo "from: $from"
      #         to=$(readlink "$p" | sed "s@/home/${old_name}/@/home/${new_name}/@g")
      #         echo "to: $to"
      #
      #         ln -sf "$to" "$from"
      #     }; fi
      # }; done

      uid = 1000;
    });

    users.samuelsung = mkIf (elem "samuelsung" cfg.users) (samuelsung_common // {
      uid = 1001;
    });

    users.root = {
      initialPassword = "p@ssw0rd";
    };

    mutableUsers = true;
    defaultUserShell = pkgs.zsh;

    groups = {
      # To change gid, run
      # groupmod -g <new_gid> foo # assign new gid to the group
      # find / -group <old_gid> -exec chgrp -h foo {} \; # apply the new gid to files
      samuelsung_common = {
        gid = 996;
      };
    };
  };
}
