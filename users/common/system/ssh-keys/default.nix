{ config, lib, options, ... }:

let
  inherit (config.networking) hostName;
  inherit (builtins) map hasAttr listToAttrs;
  inherit (lib) cartesianProductOfSets nameValuePair mkIf mkMerge unique foldl' elem;

  inherit (import ./utils.nix { inherit config lib; }) keyMapToThisMachine keyMapFromThisMachine getUnmappedToName;
  uniqueTo = mapper: list:
    foldl'
      (acc: e: if elem (mapper e) (map mapper acc) then acc else acc ++ [ e ])
      [ ]
      list;

  sopsFilesMatrix = cartesianProductOfSets { keyMap = keyMapFromThisMachine; type = [ "private" "public" ]; };

  usersWhoReceivePublicKeys = uniqueTo
    ({ account, ... }: account)
    (map ({ to, ... }: to) keyMapToThisMachine);
in
mkMerge [
  {
    # HACK(samuelsung): directly checking sopsFilesMatrix != [ ] in if will result infinite recursion
    # and not checking hasAttr "sops" will result no module `sops` even if mkIf is false
    # as a hack, add a assertion to warn user incase sopsFilesMatrix is non-empty and sops is forgotten.
    assertions = [
      {
        assertion = sopsFilesMatrix == [ ] || hasAttr "sops" options;
        message = "Non empty list of ssh private keys!!! Either remove it or enable sops";
      }
    ];
  }
  (mkIf (sopsFilesMatrix != [ ]) (if (hasAttr "sops" options) then {
    sops.secrets = listToAttrs (map
      ({ keyMap, type }:
        let
          inherit (keyMap) from to;
        in
        nameValuePair "ssh-${from.account}-${getUnmappedToName to}-${type}" {
          sopsFile = ./keys + "/secret-keys/${hostName}/${from.account}-to-${getUnmappedToName to}.${hostName}-host-secret.json";
          owner = from.account;
          key = type;
          path = "/home/${from.account}/.ssh/${getUnmappedToName to}${if type == "public" then ".pub" else ""}";

          mode = "0400";
        })
      sopsFilesMatrix
    );
  } else { }))
  {
    users.users = listToAttrs (map
      (user: nameValuePair user.account {
        openssh.authorizedKeys.keyFiles = [
          (./keys/public-keys + "/${hostName}/${user.account}")
          (./keys/public-keys.old + "/${hostName}/${user.account}")
        ];
      })
      usersWhoReceivePublicKeys
    );
  }
]
