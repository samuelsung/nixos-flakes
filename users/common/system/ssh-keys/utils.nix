{ config, lib }:

with lib;
let
  inherit (config.networking) hostName;
in
rec {
  keyMap = builtins.fromJSON (builtins.readFile ./keys/keymap.json);
  serviceMap = builtins.fromJSON (builtins.readFile ./keys/servicemap.json);

  getServiceMap = s: if hasAttr s serviceMap then serviceMap.${s} else [{ service = s; }];

  flattenedKeyMap = flatten (map
    ({ from, hosts, services }:
      let
        tos =
          (map (to: to // { type = "host"; }) hosts)
          ++ (flatten (map
            (service: map
              (mappedTo: (mappedTo // {
                type = "service";
                unmappedService = service;
              }))
              (getServiceMap service)
            )
            services
          ));
      in
      cartesianProductOfSets {
        from = [ from ];
        to = tos;
      }
    )
    keyMap
  );

  getToName = to:
    if (to.type == "host") then "${to.account}-${to.machine}"
    else if (to.type == "service") then "${to.service}"
    else null;

  getUnmappedToName = to:
    if (to.type == "host") then "${to.account}-${to.machine}"
    else if (to.type == "service") then "${to.unmappedService}"
    else null;

  getToPort = to:
    if (to.type == "host") then 939
    else if (to.type == "service") then (if (hasAttr "port" to) then to.port else 22)
    else null;

  getToAccount = to:
    if (hasAttr "account" to) then to.account else null;

  keyMapToThisMachine = filter ({ to, ... }: to.type == "host" && to.machine == hostName) flattenedKeyMap;
  keyMapFromThisMachine = filter ({ from, ... }: from.machine == hostName) flattenedKeyMap;
}
