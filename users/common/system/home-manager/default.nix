{ config, ... }:

{
  home-manager.useGlobalPkgs = true;
  home-manager.useUserPackages = true;

  home-manager.sharedModules = [{
    # set the same stateVersion in home-manger
    home.stateVersion = config.system.stateVersion;
  }];
}
