{ buildVimPlugin, buildVimPluginFrom2Nix, fetchFromGitHub }:

{
  formatter-nvim = buildVimPlugin {
    name = "formatter.nvim";
    src = fetchFromGitHub {
      owner = "mhartington";
      repo = "formatter.nvim";
      rev = "2b187813f63d22f572ebe406711e2d0e81272f8e";
      sha256 = "kgerYTT3jTOd2p60aEaSm1JnQHaGj6uTo+pz0ddTfG0=";
    };
  };

  nord-vim-ts = buildVimPlugin {
    name = "nord-vim";
    src = fetchFromGitHub {
      owner = "mrswats";
      repo = "nord-vim";
      rev = "40b83b1c97c608d7853d49f05cf97b970c0cb07d";
      sha256 = "086vdxwxsw8al80cymjrz33ny619qjkmlbbgkfh727crkx1iqdph";
    };
  };

  nvim-ts-autotag = buildVimPluginFrom2Nix {
    name = "nvim-ts-autotag";
    src = fetchFromGitHub {
      owner = "windwp";
      repo = "nvim-ts-autotag";
      rev = "99ba1f6d80842a4d82ff49fc0aa094fb9d199219";
      sha256 = "11vv8xlqwr9fpcsgkm7nxwgh2ski3n8k4ig9518vc5z8psqg349l";
    };
  };

  nvim-ts-context-commentstring = buildVimPluginFrom2Nix {
    name = "nvim-ts-context-commentstring";
    src = fetchFromGitHub {
      owner = "JoosepAlviste";
      repo = "nvim-ts-context-commentstring";
      rev = "fbaa9022d1483fbfd1d9062d15c5a7e1a41062db";
      sha256 = "0sryaxafx62vlr1zzlr43b96hhgjckrib4n76mx05q7vw09raxvb";
    };
  };

  vifm-vim = buildVimPlugin {
    name = "vifm.vim";
    src = fetchFromGitHub {
      owner = "vifm";
      repo = "vifm.vim";
      rev = "d174fbba22c0b3761127931c08e91c3e35171f5f";
      sha256 = "00nsqgykcda307xzg8992kxxrxfdyab8pgyj3lpzd8k05wm6w938";
    };
  };
}
