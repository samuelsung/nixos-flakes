{ extensionFromVscodeMarketplace }:

{
  arcticicestudio.nord-visual-studio-code = extensionFromVscodeMarketplace {
    name = "nord-visual-studio-code";
    publisher = "arcticicestudio";
    version = "0.15.0";
    sha256 = "066rqj8sf910n71g5njbp5g8advzqkd3g2lsg12wai902735i78c";
  };

  asvetliakov.vscode-neovim = extensionFromVscodeMarketplace {
    name = "vscode-neovim";
    publisher = "asvetliakov";
    version = "0.0.78";
    sha256 = "0a3zwp1w8ak985327cmjw28i7zlxcj1a6jwb750vd867hhqfw9bp";
  };

  fwcd.kotlin = extensionFromVscodeMarketplace {
    name = "kotlin";
    publisher = "fwcd";
    version = "0.2.22";
    sha256 = "0ncx7fm6xnpqsj90f4ksw58jlsw1knha7l3hhvzbg4k6d2bh0jha";
  };

  mathiasfrohlich.kotlin = extensionFromVscodeMarketplace {
    name = "kotlin";
    publisher = "mathiasfrohlich";
    version = "1.7.1";
    sha256 = "0zi8s1y9l7sfgxfl26vqqqylsdsvn5v2xb3x8pcc4q0xlxgjbq1j";
  };

  redhat.vscode-xml = extensionFromVscodeMarketplace {
    name = "vscode-xml";
    publisher = "redhat";
    version = "0.16.1";
    sha256 = "04c8fj2ng7fiafib2p7gxyxw1l7mvys0057wwgqgyasrq9i7jd3g";
  };

  rust-lang.rust = extensionFromVscodeMarketplace {
    name = "rust";
    publisher = "rust-lang";
    version = "0.7.8";
    sha256 = "039ns854v1k4jb9xqknrjkj8lf62nfcpfn0716ancmjc4f0xlzb3";
  };

  vscjava.vscode-java-debug = extensionFromVscodeMarketplace {
    name = "vscode-java-debug";
    publisher = "vscjava";
    version = "0.33.1";
    sha256 = "1spi2r17d8fyi4pfn73zs4iwimq89acfci0pgq9lcjf9fb6f2sgg";
  };

  vscjava.vscode-java-dependency = extensionFromVscodeMarketplace {
    name = "vscode-java-dependency";
    publisher = "vscjava";
    version = "0.18.3";
    sha256 = "08a1xqialawzqfaz0l48rc34jz7yb4w26nd6lpdnjcjm89s3zx3j";
  };

  vscjava.vscode-java-test = extensionFromVscodeMarketplace {
    name = "vscode-java-test";
    publisher = "vscjava";
    version = "0.29.0";
    sha256 = "0s7x6ssllasmvf0vy4ng5s4mmmdsv76c507vlh7h8xllzbalxgc8";
  };

  vscjava.vscode-maven = extensionFromVscodeMarketplace {
    name = "vscode-maven";
    publisher = "vscjava";
    version = "0.21.2";
    sha256 = "1ivmxf41ywxaihmqrg4l6qxrj5x9xag126s4lfzmgnb9cl86f489";
  };

  wayou.vscode-todo-highlight = extensionFromVscodeMarketplace {
    name = "vscode-todo-highlight";
    publisher = "wayou";
    version = "1.0.4";
    sha256 = "0s925rb668spv602x6g7sld2cs5ayiq7273963v9prvgsr0drlrr";
  };
}
