{ self
, dmenu
, dwm
, final-fantasy-xiv-sync
, flake-utils
, home-manager
, music-playlist
, neovim-flake
, nixos-generators
, nixos-hardware
, nixpkgs
, nixpkgs-master
, nixpkgs-unstable
, nur
, scripts
, slock
, sops-nix
, st
}:

let
  localScripts = (import ./scripts {
    inherit nixpkgs flake-utils;
  }).packages;

  overlays = import ./overlays {
    inherit neovim-flake;
  };

  config-packages = p: flake-utils.lib.eachDefaultSystem (system: {
    packages = import p {
      inherit system;
      config = { allowUnfree = true; };
    };
  });

  nixpkgs-alt-overlay = final: super:
    ({
      inherit music-playlist;
      dmenu = dmenu.packages.${super.system}.dmenu;
      dwm = dwm.packages.${super.system}.dwm;
      final-fantasy-xiv-sync = final-fantasy-xiv-sync.packages.${super.system}.final-fantasy-xiv-sync;
      st = st.packages.${super.system}.st;
      master = (config-packages nixpkgs-master).packages.${super.system};
      unstable = (config-packages nixpkgs-unstable).packages.${super.system};
    }
    // (overlays.neovim final super)
    // (scripts.overlay final super)
    // (slock.overlay final super));

  # TODO: Move to a separate repository
  custom-package-overlay = self: super: {
    custom.figma = self.callPackage ./packages/figma-appimage.nix { };

    custom.jdtls = self.callPackage ./packages/jdtls.nix { };

    custom.nodePackages = self.callPackage ./packages/nodePackages { };

    custom.osu = self.callPackage ./packages/osu.nix { };

    custom.vifmimg = self.callPackage ./packages/vifmimg.nix { };

    custom.vimPlugins = self.callPackage ./packages/vimPlugins.nix {
      inherit (self.vimUtils)
        buildVimPlugin
        buildVimPluginFrom2Nix;
    };

    custom.vscode-extensions = self.callPackage ./packages/vscodeExtensions.nix {
      inherit (self.vscode-utils)
        extensionFromVscodeMarketplace;
    };

    custom.python3Packages = self.callPackage ./packages/python3Packages {
      inherit (self.python3Packages)
        buildPythonPackage
        setuptools;
    };
  };

  myLib = import ./lib {
    inherit (nixpkgs.lib) nixosSystem;
    inherit (nixos-generators) nixosGenerate;
    inherit
      custom-package-overlay
      home-manager
      nixpkgs
      nixpkgs-alt-overlay
      nur
      sops-nix;
  };

  machines = import ./machines {
    inherit (myLib) mkSystem;
    inherit nixos-hardware;
  };

  inherit (machines) nixosConfigurations nixosImages;

in

(flake-utils.lib.eachDefaultSystem (
  system:
  let
    pkgs = nixpkgs.legacyPackages.${system};
    inherit (pkgs.lib) attrNames concatStringsSep mapAttrsToList flatten;
  in
  {
    apps.nixos-build-all =
      let
        buildCommand = host: ''
          nix build ".#nixosConfigurations.${host}.config.system.build.toplevel" -o target/nixosConfigurations-${host};
        '';
        hosts = attrNames nixosConfigurations;
        commends = map (buildCommand) hosts;
      in
      {
        type = "app";
        program = toString (pkgs.writeScript "nixos-build-all" (''
          #!${pkgs.runtimeShell}
          set -eu -o pipefail -x
        '' + concatStringsSep "\n" commends));
      };

    apps.nixos-images-build-all =
      let
        buildCommand = host: ''
          nix build ".#${host}" -o target/nixosImages-${host};
        '';
        hosts =
          if (builtins.hasAttr system nixosImages)
          then (attrNames nixosImages.${system})
          else [ ];
        commends = map (buildCommand) hosts;
      in
      {
        type = "app";
        program = toString (pkgs.writeScript "nixos-images-build-all" (''
          #!${pkgs.runtimeShell}
          set -eu -o pipefail -x
        '' + concatStringsSep "\n" commends));
      };

    apps.shellcheck-all =
      let
        inherit (localScripts.${system}) get_all_script_name;
      in
      {
        type = "app";
        program = toString (pkgs.writeScript "shellcheck-all" ''
          #!${pkgs.runtimeShell}

          cd ${./.}

          rtn=$(${get_all_script_name}/bin/get_all_script_name)
          IFS=';' read -ra VALUES <<< "$rtn"

          echo ""
          echo "Different File:"
          ${pkgs.shellcheck}/bin/shellcheck "''${VALUES[@]}"
        '');
      };

    apps.shfmt-check-all =
      let
        inherit (localScripts.${system}) get_all_script_name;
      in
      {
        type = "app";
        program = toString (pkgs.writeScript "shfmt-check-all" ''
          #!${pkgs.runtimeShell}

          cd ${./.}

          rtn=$(${get_all_script_name}/bin/get_all_script_name)
          IFS=';' read -ra VALUES <<< "$rtn"

          echo ""
          echo "Different File:"
          ${pkgs.shfmt}/bin/shfmt -ci -s -bn -i 2 -l "''${VALUES[@]}"
        '');
      };

    apps.updatekeys = {
      type = "app";
      program = toString (pkgs.writeScript "updatekeys" ''
        #!${pkgs.runtimeShell}

        ${pkgs.gnused}/bin/sed '/path_regex/!d;s/.*path_regex: //g' .sops.yaml | while read -r p; do {
          ${pkgs.findutils}/bin/find . | ${pkgs.gnugrep}/bin/grep "$p" | while read -r f; do {
            ${pkgs.sops}/bin/sops updatekeys -y "$f"
          }; done;
        }; done;
      '');
    };
  }
)) // {
  inherit nixosConfigurations;

  packages = nixosImages;
}
