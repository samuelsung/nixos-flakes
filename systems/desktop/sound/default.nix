{ config, lib, pkgs, ... }:

with lib;
let
  cfg = config.systemCustomize;
in
mkIf (cfg.type == "desktop") {
  environment.systemPackages = with pkgs; [
    pulsemixer
    playerctl
  ];

  # Sound Support
  sound.enable = true; # enable ALSA
  hardware.pulseaudio = {
    enable = true;
    package = pkgs.pulseaudioFull;
  };
}
