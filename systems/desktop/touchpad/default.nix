{ config, lib, ... }:

with lib;
let
  cfg = config.systemCustomize;
in
mkIf (cfg.type == "desktop") {
  # Touchpad Support
  services.xserver.libinput = {
    enable = true;
    touchpad = {
      tapping = true;
      naturalScrolling = false;
      disableWhileTyping = true;
    };
  };
}
