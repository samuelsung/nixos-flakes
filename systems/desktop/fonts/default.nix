{ config, lib, pkgs, ... }:

with lib;
let
  cfg = config.systemCustomize;
in
mkIf (cfg.type == "desktop") {
  fonts.fonts = with pkgs; [
    fira-code
    (nerdfonts.override {
      fonts = [ "FiraCode" ];
    })
    noto-fonts-cjk
  ];
}
