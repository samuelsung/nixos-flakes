{ lib, config, pkgs, ... }:

with builtins;
with lib;

let
  cfg = config.systemCustomize;
  normalUsers = attrNames (filterAttrs (_: v: v.isNormalUser) config.users.users);
in
mkIf (cfg.type == "desktop" && cfg.backlightCtl) {
  programs.light.enable = true;

  system.activationScripts.setLightMinimum = ''
    for u in ${concatStringsSep " " normalUsers}; do {
      ${pkgs.sudo}/bin/sudo -u "$u" ${pkgs.light}/bin/light -N 1 # set minimum brightness
    }; done;
  '';
}
