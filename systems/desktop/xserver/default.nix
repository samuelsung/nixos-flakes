{ config, lib, options, ... }:

with lib;
let
  cfg = config.systemCustomize;
in
mkIf (cfg.type == "desktop") {
  services.xserver.enable = true;
  services.xserver.displayManager.startx.enable = true;
  services.xserver.windowManager.xmonad.enable = true;

  services.autorandr.enable = true;
  # services.xserver.videoDrivers = options.services.xserver.videoDrivers.default ++ [ "intel" ];

  # Configure keymap in X11
  # services.xserver.layout = "us";
  # services.xserver.xkbOptions = "eurosign:e";
}
