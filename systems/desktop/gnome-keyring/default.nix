{ config, lib, ... }:

with lib;
let
  cfg = config.systemCustomize;
in
mkIf (cfg.type == "desktop") {
  services.gnome.gnome-keyring.enable = true;
  programs.seahorse.enable = true;
  security.pam.services = {
    login = {
      enableGnomeKeyring = true;
    };
  };
}
