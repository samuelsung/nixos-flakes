{ config, lib, pkgs, ... }:

with lib;
let
  cfg = config.systemCustomize;
in
mkIf (cfg.type == "desktop") {
  i18n.inputMethod.enabled = "ibus";
  i18n.inputMethod.ibus.engines = (with pkgs.ibus-engines; [
    mozc
  ]) ++ (with pkgs.unstable.ibus-engines; [
    rime
  ]);
}
