{ config, lib, ... }:

with lib;

let
  cfg = config.systemCustomize;
in
mkIf (cfg.type == "desktop") {
  # dconf
  programs.dconf.enable = true;
}
