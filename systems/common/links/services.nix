{ config, pkgs, lib, ... }:

let
  cfg = config.services.links;

  inherit (builtins)
    attrValues
    concatStringsSep
    map;
  inherit (lib)
    mapAttrsToList
    mkIf
    toposort;

  toposortedLinks = (toposort
    (a: b: a.target == b.source)
    (attrValues cfg.links)).result;

  configureScript = pkgs.writeShellScriptBin "links-configure" ''
    ${concatStringsSep "\n" (map (link: ''
      [ ! -e "${link.target}" ] && [ ! -h "${link.target}" ] \
        && ln ${if !link.hardLink then "-s" else ""} "${link.source}" "${link.target}" \
        && echo "${if !link.hardLink then "sym" else "hard"} link ${link.target} linked." \
        || echo "file target ${link.target} exists, skip."
    '')
    toposortedLinks)}
  '';

in
{
  config = mkIf cfg.enable {
    systemd.services.links-configure = {
      description = "links configuration";
      documentation = [ ];
      after = [ "local-fs.target" "remote-fs.target" ];
      requires = [ "local-fs.target" "remote-fs.target" ];
      wantedBy = [ "multi-user.target" ];
      restartIfChanged = true;
      serviceConfig = {
        Type = "oneshot";
        RemainAfterExit = true;
        ExecStart = "${configureScript}/bin/links-configure";
      };
    };
  };
}
