{ config, lib, ... }:

let
  cfg = config.services.links;

  inherit (lib)
    mkEnableOption
    mkOption
    types;

  linkModule = types.submodule
    ({ name, config, ... }: {
      options = {
        target = mkOption {
          default = name;
          type = types.path;
          description = ''
            the path of the target
          '';
        };

        source = mkOption {
          type = types.path;
          description = ''
            the path of the source
          '';
        };

        hardLink = mkOption {
          type = types.bool;
          default = false;
          description = ''
            whether it is a symlink or a hard link
          '';
        };
      };
    });
in
{
  options.services.links = {
    enable = mkEnableOption "links";

    links = mkOption {
      default = { };
      description = ''
        list of links that needs to be added
      '';
      type = types.attrsOf linkModule;
    };
  };
}
