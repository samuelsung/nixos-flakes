{ options, config, lib, ... }:

with lib;
let
  cfg = config.systemCustomize;

  inherit (cfg.timeServers) servers acceptHighRootDistanceTimeServers;
in
{
  networking.timeServers = mkIf (servers != [ ]) servers;

  services.timesyncd.extraConfig =
    mkIf acceptHighRootDistanceTimeServers
      "RootDistanceMaxSec=30";
}
