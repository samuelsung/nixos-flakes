# sopsFile structure
# {
#   restic = {
#     ${server_name} = {
#       server = `restic server url (rest:https://user:pass@host:8000/my_backup_repo)`;
#       repo = `repo password`;
#     }
#   }
# }

{ config, lib, ... }:

with builtins;
with lib;

let
  cfg = config.systemCustomize;

  encryptedJson = (fromJSON (readFile cfg.resticClientSopsFile)).restic;

  serverNames = attrNames encryptedJson;

  matrix = cartesianProductOfSets { serverName = serverNames; type = [ "server" "repo" ]; };

  namedSopsSecretList = map
    ({ serverName, type }:
      nameValuePair "restic-client-${serverName}-${type}" {
        sopsFile = cfg.resticClientSopsFile;
        mode = "0400";
        key = "restic/${serverName}/${type}";
      }
    )
    matrix;
in
mkIf (cfg.resticClientSopsFile != null) {
  sops.secrets = listToAttrs namedSopsSecretList;
}
