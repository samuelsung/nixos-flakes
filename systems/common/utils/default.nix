{ config, lib, pkgs, ... }:

with lib;
let
  cfg = config.systemCustomize;
in
mkMerge [
  # Common utils
  {
    environment.systemPackages = with pkgs; [
      # compression
      gzip
      zip
      unzip
      unrar
      dpkg
      cpio

      # utils
      git
      wget
      curl
      jq
      coreutils-full
      dbus # read dbus singal
      pciutils # lspci...
      psmisc # killall, pstree...
      fzf # search
      openssl
      htop
      acl # for file permission
      file # get file type
      p7zip # 7z

      # File Manager
      (moreutils.overrideAttrs (oldAttrs: rec {
        preBuild = oldAttrs.preBuild + ''
          # add space padding
          substituteInPlace vidir --replace "print OUT \"\$c\t\$_\\n\";" "\$d=sprintf(\"%5d\", \$c); print OUT \"\$d \$_\\n\"";
          substituteInPlace vidir --replace "if (/^(\\d+)\\t{0,1}(.*)/) {" "if(/^\\s*(\\d+) {0,1}(.*)/) {";
        '';
      }))

      # nixos
      home-manager

      # secrets
      sops
      ssh-to-age
      age

      # backups
      restic
    ];
  }

  # Desktop utils
  (mkIf (cfg.type == "desktop") {
    environment.systemPackages = with pkgs; [
      # webcam
      v4l-utils # control webcam params

      # multimedia
      (ffmpeg-full.override {
        # ffmpeg with libbluray enabled
        libbluray = pkgs.libbluray.override {
          withAACS = true;
          withBDplus = true;
        };
      })

      # mount
      ntfs3g # NTFS
      fuse # NTFS
      udisks # mount usb

      # nixos
      appimage-run
      steam-run

      # miscellaneous
      termdown # tui timer
      ssh-keygen-from-keymap

      # makemkv

      # X related utils
      xsel
      xorg.xwininfo
      xorg.xdpyinfo
      xorg.xbacklight
      xclip
      xdotool
      unclutter-xfixes # hide cursor if inactive
      slock
      xss-lock

      # Remote
      remmina
      libvncserver
      freerdp

      # Commutation
      teams
      discord
      zoom-us
      signal-desktop

      # Mail
      protonmail-bridge
      thunderbird

      # Configurations
      lxappearance
      lxrandr
      xlayoutdisplay

      # Browser
      firefox
      ungoogled-chromium

      # Terminal
      xterm
      alacritty

      # File Manager
      gnome3.nautilus
      calibre
      custom.vifmimg

      # Image Viewer
      sxiv

      # Documents
      zathura
      libreoffice
      joplin-desktop

      # Multimedia
      picard
      deadbeef-with-plugins
      mpv
      mkvtoolnix
      (vlc.override {
        libbluray = pkgs.libbluray.override {
          withAACS = true;
          withBDplus = true;
        };
      })
      handbrake

      # Image
      imagemagick
      ffmpegthumbnailer
      poppler_utils
      fontpreview

      # Design
      drawio
    ];

    programs.slock.enable = true;
  })

  # Development utils
  (mkIf (cfg.type == "desktop") {
    environment.systemPackages = with pkgs; [
      # utils
      unstable.glab # gitlab cli tool (only available on unstable as of 2021 Mar 10)

      # management
      linode-cli

      # LaTeX
      texlive.combined.scheme-full

      # C
      gcc
      gnumake
      gnupatch
      libtool
      autoconf
      automake
      pkgconf
      texinfo
      binutils

      # Javascript
      nodejs
      yarn
      nodePackages.expo-cli
      nodePackages.node2nix

      # Python
      python3Full # Note that there is no need to ust pip in Nixos https://nixos.wiki/wiki/Python

      # Rust
      cargo
      rustc
      rustup

      # Go
      go
      vgo2nix

      # Java
      # jdtls

      # sql
      sqlite

      # docker
      docker-compose

      # git wiki
      gollum

      # android
      android-studio

      # Elm
      elmPackages.elm

      # Haskell
      haskellPackages.haskell-language-server
      haskellPackages.cabal-install
      ghc

      # nix
      patchelf

      # Design
      custom.figma

      # Database
      dbeaver

      # IDE
      vscodium
    ];

    # docker
    virtualisation.docker.enable = true;

    # adb
    programs.adb.enable = true;

    # Java
    programs.java = {
      enable = true;
      package = pkgs.openjdk11;
    };
  })
]
