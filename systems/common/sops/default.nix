{ config, lib, ... }:

with lib;

let
  cfg = config.systemCustomize;
in
mkIf (cfg.type != "iso") {
  sops.defaultSopsFile = ../../../common.secret.yaml;
  sops.age.sshKeyPaths = [ "/etc/ssh/ssh_host_ed25519_key" ];
}
