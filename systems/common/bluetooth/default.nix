{ config, lib, pkgs, ... }:

with lib;
let
  cfg = config.systemCustomize;
in
mkIf (elem cfg.type [ "desktop" "iso" ]) {
  # Bluetooth
  hardware.bluetooth = {
    enable = true;
    powerOnBoot = true;
    package = pkgs.bluezFull;
  };
}
