{ neovim-flake }:

{
  neovim = import ./neovim { inherit neovim-flake; };
}
