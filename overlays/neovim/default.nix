{ neovim-flake }:

final: super:

let
  fullPackages = neovim-flake.neovimBuilder.${super.system} {
    config.vim = {
      lsp.enableAllLanguage = true;
    };
  };

  slimPackages = neovim-flake.neovimBuilder.${super.system} {
    config.vim = {
      lsp = {
        enableAllLanguage = false;
        nix.enable = true;
      };
    };
  };
in
{
  neovim = fullPackages.neovim;
  nvimpager = fullPackages.nvimpager;
  neovimSlim = slimPackages.neovim;
  nvimpagerSlim = slimPackages.nvimpager;
}
